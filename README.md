# FagprojektWSA
Repository for fagprojekt 2021 WSA 

Projekt udført med python 3.8


## Data filtering

### data_class.py 
Where the data object is defined. Used to store and filter the data.

### create_statespace_class.py 
Where the space object is defined, holding all the possible states, intentions and environments.

## Cross validation 

### CV_eval_stratified.py
Cross validation using stratified 10-fold.

### evaluate_models_funcs.py
Where the RMSE and accuracy functions are defined.

### json_funcs.py
Function to write and read the cross validation results to json files.

## The models

### Baseline_models_funcs_count.py
Where the 3 baseline models are defined. Also where the backup-baseline is.

### similarity_funcs.py
Where the user-based model is defined.

### matrix_fac_funcs.py
Where the non-negative matrix factorisation model is defined.

### transform_functions.py
Transforms settings to non-negative and back again for the matrix factorisation model.

### nmf.py
The HALS algorithm for the matrix factorisation function.
from Script from https://github.com/kimjingu/nonnegfac-python/blob/master/nonnegfac/nmf.py

### matrix_utils.py
Used by the nmf.py script.
from Script from https://github.com/kimjingu/nonnegfac-python/blob/master/nonnegfac/nmf.py




## Visualisation of data

### CV_plot_funcs.py
Functions to plot the result plots.

### plot_results_notebook.ipynb
Plots of the results.

### data_pre_clean.ipynb
Notebook with the data exploration.

### data_post_clean.ipynb
Notebook with the data exploration post running the data clean functions.





## The folders

### CV_files
Has the output of the cross validations in json format


### not final
Contains things that ended up not being in the final report.

### cluster
Has the notebooks exploring cluster analysis - also did not make it to the final report