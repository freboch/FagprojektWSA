import numpy as np


def transform_data(direction, settings):
    """
    Transforms the list of settings.
    If direction = 'positive' the settings are transformed from ranges [[-12:12] , [-12:6] , [-12:6]] to [[0:24] , [0:18] , [0:18]]
    if direction = 'negative' the settings are transformed from ranges [[0:24] , [0:18] , [0:18]] to [[-12:12] , [-12:6] , [-12:6]]

    :param direction: "positive" to get only positive settings. "negative" to get back to normal settings
    :param settings: list of arrays with [bass, midrange, treble] setting values
    :return: list of arrays with the transformed settings
    """
    # convert list to array
    data_arr = np.asarray(settings)
    # transform
    if direction == 'positive':
        data_arr = data_arr + 13
        # convert back to list
        data_list = list(data_arr)
    elif direction == 'negative':
        data_arr = data_arr - 13
        # convert back to list
        data_list = list(data_arr)
        # the values that become -13 should be transformed to nan as they would be outside the range
        # if there is only one setting:
        if len(data_arr.shape) == 1:
            if np.any(data_arr < -12) or np.any(data_arr[0] > 12) or np.any(data_arr[1] > 6) or np.any(data_arr[2] > 6):
                data_list = np.nan
        # if there is a list of settings:
        else:
            for i, sett in enumerate(data_list):
                if np.any(sett < -12) or np.any(sett[0] > 12) or np.any(sett[1] > 6) or np.any(sett[2] > 6):
                    data_list[i] = np.nan
    return data_list


