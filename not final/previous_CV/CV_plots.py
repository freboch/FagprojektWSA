import matplotlib.pyplot as plt
import numpy as np
import json
import os

def open_json(name,directory):
    path = os.path.join(directory,name+'.json')
    file = open(path)
    data = json.loads(file.read())
    return data

B1_Tr = open_json('B1_Tr','CV_files')
B1_Te = open_json('B1_Te','CV_files')
B2_Tr = open_json('B2_Tr','CV_files')
B2_Te = open_json('B2_Te','CV_files')
B3_Tr = open_json('B3_Tr','CV_files')
B3_Te = open_json('B3_Te','CV_files')
Tr_acc = open_json('Tr_acc','CV_files')
Te_acc = open_json('Te_acc','CV_files')



def plot_Train(Train_errorsB1, Train_errorsB2, Train_errorsB3):
    x = ["Bass","Mid range","Treble"]

    plt.plot(x,np.mean(Train_errorsB1, 0),'.', color='#ff66b3', markersize = 8)
    plt.errorbar(x, np.mean(Train_errorsB1, 0),fmt='none', yerr=2*np.std(Train_errorsB1), ecolor='#cc0066')

    plt.plot(x,np.mean(Train_errorsB2, 0),'.', color='#00e6e6', markersize = 8)
    plt.errorbar(x, np.mean(Train_errorsB2, 0),fmt='none', yerr=2*np.std(Train_errorsB2),ecolor='#006666')

    plt.plot(x, np.mean(Train_errorsB3, 0), '.', color='#ffa366', markersize=8)
    plt.errorbar(x, np.mean(Train_errorsB3, 0), fmt='none', yerr=2 * np.std(Train_errorsB3), ecolor='#ff6600')

    plt.grid(color='black', linestyle='-', linewidth=0.5)
    plt.xlabel('Setting',fontsize=16,fontstyle='italic')
    plt.ylabel('Root Mean Square Error',fontsize=16,fontstyle='italic')

    plt.title('Train errors - Baseline models',fontsize=20,fontstyle='oblique')
    plt.legend(['BL Train', 'RC Train','BC Train'], loc ="upper right")
    plt.savefig('Plots\Error_trainplot.png',dpi=600)
    plt.show()

def plot_Test(Test_errorsB1,Test_errorsB2,Test_errorsB3):
    x = ["Bass","Mid range","Treble"]

    plt.plot(x, np.mean(Test_errorsB1, 0), '.', color='#ff66b3', markersize=8)
    plt.errorbar(x, np.mean(Test_errorsB1, 0), fmt='none', yerr=2 * np.std(Test_errorsB1), ecolor='#cc0066')

    plt.plot(x, np.mean(Test_errorsB2, 0), '.', color='#00e6e6', markersize=8)
    plt.errorbar(x, np.mean(Test_errorsB2, 0), fmt='none', yerr=2 * np.std(Test_errorsB2), ecolor='#006666')

    plt.plot(x, np.mean(Test_errorsB3, 0), '.', color='#ffa366', markersize=8)
    plt.errorbar(x, np.mean(Test_errorsB3, 0), fmt='none', yerr=2 * np.std(Test_errorsB3), ecolor='#ff6600')

    plt.grid(color='black', linestyle='-', linewidth=0.5)
    plt.xlabel('Setting',fontsize=16,fontstyle='italic')
    plt.ylabel('Root Mean Square Error',fontsize=16,fontstyle='italic')

    plt.title('Test errors - Baseline models',fontsize=20,fontstyle='oblique')
    plt.legend(['BL Test', 'RC Test','BC Test'], loc ="upper right")
    plt.savefig('Plots\Error_testplot.png',dpi=600)
    plt.show()

def train_acc(Tr_acc):
    Te_acc = np.asarray(Tr_acc)
    x = [0,1,2]

    plt.plot(x[0], np.mean(Te_acc[:,0]), '.', color='#ff66b3', markersize=8)
    plt.errorbar(x[0], np.mean(Te_acc[:,0]), fmt='none', yerr=2 * np.std(Te_acc[:,0]), ecolor='#cc0066')

    plt.plot(x[1], np.mean(Te_acc[:,1]), '.', color='#00e6e6', markersize=8)
    plt.errorbar(x[1], np.mean(Te_acc[:,1]), fmt='none', yerr=2 * np.std(Te_acc[:,1]), ecolor='#006666')

    plt.plot(x[2], np.mean(Te_acc[:,2]), '.', color='#ffa366', markersize=8)
    plt.errorbar(x[2], np.mean(Te_acc[:,2]), fmt='none', yerr=2 * np.std(Te_acc[:,2]), ecolor='#ff6600')

    plt.grid(color='black', linestyle='-', linewidth=0.5)
    plt.ylabel('Accuracy',fontsize=16,fontstyle='italic')
    plt.xticks([0,1,2],['Mean BL','RC BL','BC BL'],rotation=20)
    plt.title('Train accuracy Baseline models',fontsize=20,fontstyle='oblique')
    plt.legend(['Mean', 'Random Corner','Best Corner'], loc ="lower right")
    plt.savefig('Plots\Train_accuracy.png',dpi=600)
    plt.show()


def test_acc(Te_acc):
    Te_acc = np.asarray(Te_acc)
    x = [0,1,2]

    plt.plot(x[0], np.mean(Te_acc[:,0]), '.', color='#ff66b3', markersize=8)
    plt.errorbar(x[0], np.mean(Te_acc[:,0]), fmt='none', yerr=2 * np.std(Te_acc[:,0]), ecolor='#cc0066')

    plt.plot(x[1], np.mean(Te_acc[:,1]), '.', color='#00e6e6', markersize=8)
    plt.errorbar(x[1], np.mean(Te_acc[:,1]), fmt='none', yerr=2 * np.std(Te_acc[:,1]), ecolor='#006666')

    plt.plot(x[2], np.mean(Te_acc[:,2]), '.', color='#ffa366', markersize=8)
    plt.errorbar(x[2], np.mean(Te_acc[:,2]), fmt='none', yerr=2 * np.std(Te_acc[:,2]), ecolor='#ff6600')

    plt.grid(color='black', linestyle='-', linewidth=0.5)
    plt.ylabel('Accuracy',fontsize=16,fontstyle='italic')
    plt.xticks([0,1,2],['Mean BL','RC BL','BC BL'],rotation=20)
    plt.title('Test accuracy Baseline models',fontsize=20,fontstyle='oblique')
    plt.legend(['Mean', 'Random Corner','Best Corner'], loc ="lower right")
    plt.savefig('Plots\Test_accuracy.png',dpi=600)
    plt.show()

#plot_Train(B1_Tr,B2_Tr,B3_Tr)
#plot_Test(B1_Te,B2_Te,B3_Te)
#test_acc(Te_acc)
#train_acc(Tr_acc)
#Tr_acc = np.asarray(Tr_acc)
#print(2*np.std(Tr_acc[:,0]))

