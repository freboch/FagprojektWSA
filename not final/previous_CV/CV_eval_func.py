from create_statespace_class import Space
from data_class import *
from sklearn.model_selection import KFold # import KFold
from B1 import *
from RC_eval import *
from BC_3pred import *
import json
import os

app = "EVOKE"
k = 10
# import data and create lists for all 4 files and combine them
paths = ['part-00000-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5531-1-c000.json',
         'part-00001-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5532-1-c000.json',
         'part-00002-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5533-1-c000.json',
         'part-00003-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5534-1-c000.json']

space = Space(app)
data = Data(paths,app)
data.clean()
def trans(data):
    #To array
    data_IDs = np.asarray(data.IDs)
    data_states = np.asarray(data.states)
    data_settings = np.asarray(data.settings)

    return data_IDs, data_states, data_settings

data_IDs, data_states, data_settings = trans(data)

def CV(data_IDs, data_states, data_settings,space,k):
    # List of errors
    Train_errorsB1 = []
    Test_errorsB1 = []
    Train_errorsB2 = []
    Test_errorsB2 = []
    Train_errorsB3 = []
    Test_errorsB3 = []
    Train_acc_CV = []
    Test_acc_CV = []

    # Define the splits
    kf = KFold(n_splits=k)
    fold = 1
    # Get the number of splitting iterations for the data set
    kf.get_n_splits(data_IDs)
    KFold(n_splits=k, random_state=None, shuffle=False)

    # Split into test and training sets
    for train_index, test_index in kf.split(data_IDs):
        print('CV fold {}'.format(fold))
        ID_train, state_train, settings_train = data_IDs[train_index], data_states[train_index], data_settings[
            train_index]
        ID_test, state_test, settings_test = data_IDs[test_index], data_states[test_index], data_settings[test_index]

        IDs = list(set(ID_train))
        # create the datamatrix with the training data user IDs and the total state space
        data_matrix = np.ones((3, len(IDs), len(space.states)))
        data_matrix[:] = None
        for i, state in enumerate(state_train):
            idx0 = IDs.index(ID_train[i])
            idx1 = space.states.index(state)
            # insert the setting in the data matrix
            data_matrix[:, idx0, idx1] = settings_train[i]


        """train and test model"""

        #Baseline 1
        train_MSE, test_MSE,train_acc_B1, test_acc_B1 = Baseline1(data_matrix, ID_train,state_train,settings_train, ID_test, state_test, settings_test,space)

        #Append to list
        Train_errorsB1.append(train_MSE)

        Test_errorsB1.append(test_MSE)

        #Random Corner Baseline

        train_eB2, test_eB2, train_acc_B2, test_acc_B2 = random_corner(ID_train,ID_test, settings_train,settings_test)

        Train_errorsB2.append(train_eB2)
        Test_errorsB2.append(test_eB2)

        #Best Corner Baseline
        dic = best_corner_dict(space.states, state_train, settings_train)

        train_eB3, test_eB3,train_acc_B3,test_acc_B3 = best_corner_eval(dic, state_train, state_test, settings_train, settings_test)
        Train_errorsB3.append(train_eB3)
        Test_errorsB3.append(test_eB3)

        Train_acc = list(np.array([train_acc_B1, train_acc_B2, train_acc_B3])/len(settings_train))
        Test_acc = list(np.array([test_acc_B1, test_acc_B2, test_acc_B3])/len(settings_test))

        Train_acc_CV.append(Train_acc)
        Test_acc_CV.append(Test_acc)
        fold += 1
    return Train_errorsB1, Test_errorsB1, Train_errorsB2, Test_errorsB2, Train_errorsB3, Test_errorsB3, Train_acc_CV, Test_acc_CV

Train_errorsB1, Test_errorsB1, Train_errorsB2, Test_errorsB2,Train_errorsB3, Test_errorsB3, Train_acc_CV, Test_acc_CV = CV(data_IDs, data_states,data_settings,space,k)

def write_json(file,name,directory):
    json_file = json.dumps(file)
    path = os.path.join(directory,name+'.json')
    f = open(path, 'w')
    f.write(json_file)
    f.close()

write_json(list(Train_errorsB1),'B1_Tr','CV_files')
write_json(list(Test_errorsB1),'B1_Te','CV_files')
write_json(list(Train_errorsB2),'B2_Tr','CV_files')
write_json(list(Test_errorsB2),'B2_Te','CV_files')
write_json(list(Train_errorsB3),'B3_Tr','CV_files')
write_json(list(Test_errorsB3),'B3_Te','CV_files')
write_json(list(Train_acc_CV),'Tr_acc','CV_files')
write_json(list(Test_acc_CV),'Te_acc','CV_files')

