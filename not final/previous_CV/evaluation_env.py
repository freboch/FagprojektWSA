from data_class import *
from create_statespace_class import *

import numpy as np

np.random.seed(42)
# choose which app to look at between 'EVOKE' and 'MOMENT'
app = "EVOKE"


# get the data and create the data object
paths = ['part-00000-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5531-1-c000.json',
         'part-00001-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5532-1-c000.json',
         'part-00002-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5533-1-c000.json',
         'part-00003-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5534-1-c000.json']
data = Data(paths, app)

# clean the data
data.clean()

# create the state space
space = Space(app)


# Splitte Data i træning og test (cross-val eller hold out)

### Modeller trænes og tests på Data
# mean modellen (lort)
# random corner model
# best setting (for environment) model

# Evaluering af modeller; hver især og sammenligning

# plot/tabel output til nem aflæsning af resultatet