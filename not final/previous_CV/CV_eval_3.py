from data_class import *
from create_statespace_class import *
from Baseline_models_funcs import *
from evaluate_models_funcs import *
from similarity_funcs import *
from json_funcs import *

import numpy as np
from sklearn.model_selection import KFold # import KFold

np.random.seed(42)

# choose which app to look at between 'EVOKE' and 'MOMENT'
app = "EVOKE"
# get the data
paths = ['part-00000-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5531-1-c000.json',
         'part-00001-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5532-1-c000.json',
         'part-00002-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5533-1-c000.json',
         'part-00003-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5534-1-c000.json']

# create the data object
data = Data(paths, app)
# create the state space
space = Space(app)
# clean the data
data.clean()
data.CF_clean()

# begin cross validation
k = 10
# Define the splits
kf = KFold(n_splits=k, random_state=42, shuffle=True)
fold = 1
# Get the number of splitting iterations for the data set
# use the unique IDs so the same user wont show up in both training and test set
data_unique_ID_array = np.asarray(data.unique_IDs)
kf.get_n_splits(data_unique_ID_array)

data_states_array = np.asarray(data.states)
data_settings_array = np.asarray(data.settings)
data_ID_array = np.asarray(data.IDs)

# Lists for collecting errors
Train_RMSEs_mean = []
Test_RMSEs_mean = []
Train_RMSEs_best = []
Test_RMSEs_best = []
Train_RMSEs_random = []
Test_RMSEs_random = []
Train_RMSEs_similarity = []
Test_RMSEs_similarity = []

Train_accs_mean = []
Test_accs_mean = []
Train_accs_best = []
Test_accs_best = []
Train_accs_random = []
Test_accs_random = []
Train_accs_similarity = []
Test_accs_similarity = []

# run cross validation
for train_index, test_index in kf.split(data_unique_ID_array):
    print('CV fold {}'.format(fold))
    fold += 1
    # split unique IDs into training and test set
    train_IDs = data_unique_ID_array[train_index]
    test_IDs = data_unique_ID_array[test_index]
    # get training and test observation index from the IDs
    train_obs_index = [i for i, ID in enumerate(data.IDs) if ID in train_IDs]
    test_obs_index = [i for i, ID in enumerate(data.IDs) if ID in test_IDs]
    # use the index to get training and test data
    # train
    train_states = data_states_array[train_obs_index]
    train_settings = data_settings_array[train_obs_index]
    train_IDs = data_ID_array[train_obs_index]
    # test
    test_states = data_states_array[test_obs_index]
    test_settings = data_settings_array[test_obs_index]
    test_IDs = data_ID_array[test_obs_index]

    # train models on training data
    mean_dic = baseline_mean_train_3(space.states, train_states, train_settings)
    best_dic = baseline_most_popular_train_3(space.states, train_states, train_settings)
    sim_matrix, train_data_matrix = train_similarity(train_IDs, train_states, train_settings)

    # get model predictions
    # train predictions
    train_mean_predictions = baseline_mean_predict_3(mean_dic, train_states)
    train_best_predictions = baseline_most_popular_predict_3(best_dic, train_states)
    train_random_predictions = baseline_random_corner_predict_3(train_states)
    train_similarity_predictions = prediction_similarity(train_IDs, train_states, sim_matrix, train_data_matrix)
    # test predictions
    test_mean_predictions = baseline_mean_predict_3(mean_dic, test_states)
    test_best_predictions = baseline_most_popular_predict_3(best_dic, test_states)
    test_random_predictions = baseline_random_corner_predict_3(test_states)
    test_similarity_predictions = prediction_similarity(test_IDs, test_states, sim_matrix, train_data_matrix)

    # compute accuracy and RMSE

    # train
    Train_accs_mean.append(evaluate_acc_3(train_settings, train_mean_predictions))
    Train_accs_best.append(evaluate_acc_3(train_settings, train_best_predictions))
    Train_accs_random.append(evaluate_acc_3(train_settings, train_random_predictions))
    Train_accs_similarity.append(evaluate_acc_3(train_settings, train_similarity_predictions))

    Train_RMSEs_mean.append(evaluate_RMSE_3(train_settings, train_mean_predictions))
    Train_RMSEs_best.append(evaluate_RMSE_3(train_settings, train_best_predictions))
    Train_RMSEs_random.append(evaluate_RMSE_3(train_settings, train_random_predictions))
    Train_RMSEs_similarity.append(evaluate_RMSE_3(train_settings, train_similarity_predictions))

    # test
    Test_accs_mean.append(evaluate_acc_3(test_settings, test_mean_predictions))
    Test_accs_best.append(evaluate_acc_3(test_settings, test_best_predictions))
    Test_accs_random.append(evaluate_acc_3(test_settings, test_random_predictions))
    Test_accs_similarity.append(evaluate_acc_3(test_settings, train_similarity_predictions))

    Test_RMSEs_mean.append(evaluate_RMSE_3(test_settings, test_mean_predictions))
    Test_RMSEs_best.append(evaluate_RMSE_3(test_settings, test_best_predictions))
    Test_RMSEs_random.append(evaluate_RMSE_3(test_settings, test_random_predictions))
    Test_RMSEs_similarity.append(evaluate_RMSE_3(test_settings, train_similarity_predictions))


# save all the error and accuracy lists to a dictionary before json dumping it
eval_data_dic = {
    'Train_RMSEs_mean': Train_RMSEs_mean,
    'Test_RMSEs_mean': Test_RMSEs_mean,
    'Train_RMSEs_best': Train_RMSEs_best,
    'Test_RMSEs_best': Test_RMSEs_best,
    'Train_RMSEs_random': Train_RMSEs_random,
    'Test_RMSEs_random': Test_RMSEs_random,
    'Train_accs_similarity': Train_accs_similarity,
    'Train_RMSEs_similarity': Train_RMSEs_similarity,
    'Train_accs_mean': Train_accs_mean,
    'Test_accs_mean': Test_accs_mean,
    'Train_accs_best': Train_accs_best,
    'Test_accs_best': Test_accs_best,
    'Train_accs_random': Train_accs_random,
    'Test_accs_random': Test_accs_random,
    'Test_accs_similarity': Test_accs_similarity,
    'Test_RMSEs_similarity': Test_RMSEs_similarity
}

# write results to json
folder = 'CV_files'
write_json(eval_data_dic, 'eval_data_dic', folder)


