import numpy as np
from create_statespace_class import Space
from data_class import Data
from baseline import baseline_train, baseline_predict
from sklearn.model_selection import KFold # import KFold


app = "EVOKE"
k = 3
# import data and create lists for all 4 files and combine them
paths = ['part-00000-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5531-1-c000.json',
         'part-00001-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5532-1-c000.json',
         'part-00002-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5533-1-c000.json',
         'part-00003-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5534-1-c000.json']

space = Space(app)
data = Data(paths,app)
def trans(data):
    #To array
    data_IDs = np.asarray(data.IDs)
    data_states = np.asarray(data.states)
    data_settings = np.asarray(data.settings)

    return data_IDs, data_states, data_settings

data_IDs, data_states, data_settings = trans(data)

def CV(data_IDs, data_states, data_settings,space,k):
    # List of errors
    Train_errors = []
    Test_errors = []

    # Define the splits
    kf = KFold(n_splits=k)

    # Get the number of splitting iterations for the data set
    kf.get_n_splits(data_IDs)
    KFold(n_splits=k, random_state=None, shuffle=False)

    # Split into test and training sets
    for train_index, test_index in kf.split(data_IDs):
        ID_train, state_train, settings_train = data_IDs[train_index], data_states[train_index], data_settings[
            train_index]
        ID_test, state_test, settings_test = data_IDs[test_index], data_states[test_index], data_settings[test_index]

        IDs = list(set(ID_train))
        # create the datamatrix with the training data user IDs and the total state space
        data_matrix = np.ones((3, len(IDs), len(space.states)))
        data_matrix[:] = None
        for i, state in enumerate(state_train):
            idx0 = IDs.index(ID_train[i])
            idx1 = space.states.index(state)
            # insert the setting in the data matrix
            data_matrix[:, idx0, idx1] = settings_train[i]

        """train and test model"""
        # train the baseline model on the training data
        trained_mean = baseline_train(data_matrix)

        # size of the train set
        n = np.size(ID_train)

        # collect errors
        train_errors = np.zeros((n, 3))
        for i in range(n):
            pred_setting = baseline_predict(state_train[i], trained_mean, space.states)
            error = (pred_setting - settings_train[i]) ** 2
            train_errors[i, :] = error

        # calculate the mean error on the train set
        train_MSE = np.sqrt(np.mean(train_errors, 0))
        Train_errors.append(train_MSE)
        # size of test set
        n = np.size(ID_test)
        # collect errors
        test_errors = np.zeros((n, 3))
        for i in range(n):
            pred_setting = baseline_predict(state_test[i], trained_mean, space.states)

            # print("the predicted setting is ", pred_setting)
            # print("the true setting was ", test_settings[i])
            error = (pred_setting - settings_test[i]) ** 2
            test_errors[i, :] = error

        # calculate the mean error on the test set
        test_MSE = np.sqrt(np.mean(test_errors, 0))
        Test_errors.append(test_MSE)

        print('train error is ', train_MSE)
        print('test error is ', test_MSE)
    return Test_errors, Train_errors

Test_errors, Train_errors = CV(data_IDs, data_states,data_settings,space,k)
print(Test_errors)
print(Train_errors)