from data_class import *
from create_statespace_class import *
from json_funcs import *

import numpy as np

np.random.seed(42)

# choose which app to look at between 'EVOKE' and 'MOMENT'
app = "EVOKE"
# get the data
paths = ['part-00000-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5531-1-c000.json',
         'part-00001-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5532-1-c000.json',
         'part-00002-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5533-1-c000.json',
         'part-00003-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5534-1-c000.json']

# create the data object
data = Data(paths, app)
# create the state space
space = Space(app)
# clean the data
data.clean()

IDS = data.unique_IDs.copy()
save_test = {'data_set': IDS}
folder = 'CV_files'
write_json(save_test, 'save_test', folder)
