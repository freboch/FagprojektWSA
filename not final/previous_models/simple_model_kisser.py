from create_statespace import create_statespace
from create_statespace_class import *
from data_class import *
import numpy as np
import pandas as pd
import math
from sklearn.metrics.pairwise import pairwise_distances
from sklearn.metrics import mean_squared_error
from math import sqrt
from sklearn.model_selection import train_test_split

np.random.seed(42)
app = "EVOKE"

# Import data and create lists for all 4 files and combine them
paths = [
    '/Users/AnneSophieHHansen/Desktop/part-00000-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5531-1-c000.json',
    '/Users/AnneSophieHHansen/Desktop/part-00001-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5532-1-c000.json',
    '/Users/AnneSophieHHansen/Desktop/part-00002-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5533-1-c000.json',
    '/Users/AnneSophieHHansen/Desktop/part-00003-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5534-1-c000.json']

# Load data
space = Space(app)
data = Data(paths, app)
data.clean()


# Transform user ids (user_id), states (item_id) and settings (sett) to an array
def trans_to_array(data):
    user_id = np.asarray(data.IDs)
    item_id = np.asarray(data.states)
    sett = np.asarray(data.settings)
    return user_id, item_id, sett


user_id, item_id, sett = trans_to_array(data)


def trans_to_positive(data):
    # Transform
    data = data + 13
    return data


# sett = trans_to_positive(sett)


# Make dataframe with all relevant variables
df = pd.DataFrame(list(zip(user_id, item_id, sett)), columns=["user_id", "item_id", "sett"])

# Filter data
# Delete unique users (if user only appears once)
df = df[df.duplicated(subset=['user_id'], keep=False)]
# Delete duplicates (if a user has more than one setting for a state)
df = df.drop_duplicates(subset=['user_id', 'item_id'], keep="last")

# Unique users and items
n_users = df.user_id.unique().shape[0]
n_items = df.item_id.unique().shape[0]
print("Number of users = " + str(n_users) + " | Number of states " + str(n_items))
print("Number of observations = " + str(len(df)))

# Compute the sparsity level
sparsity = round(1.0 - len(df) / float(n_users * n_items), 3)
print("The sparsity level is " + str(sparsity * 100) + "%")

# Split data
from sklearn.model_selection import train_test_split

train_data, test_data = train_test_split(df, test_size=0.2)

# Create user-item matrices
nan = float("NaN")
train_data_matrix = train_data.pivot(index="user_id", columns="item_id", values="sett")
test_data_matrix = test_data.pivot(index='user_id', columns='item_id', values='sett')

# Replace NaNs with array [NaN, NaN, NaN]
# x = np.array([np.nan, np.nan, np.nan])
# for column in train_data_matrix:
#     train_data_matrix[column] = train_data_matrix[column].fillna({i: x for i in train_data_matrix.index})

# Creates cosine similarity matrix for users.
# Compute the pairwise distance matrix with NaN values
from sklearn.metrics.pairwise import nan_euclidean_distances
from scipy import spatial

# len_rows = np.shape(train_data_matrix)[0]
# len_columns = np.shape(train_data_matrix)[1]
#
# similarity_matrix = np.zeros((len_rows, len_rows))      # Create empty matrix
# similarity_matrix[similarity_matrix == 0] = np.nan      # Fill it with NaN
# for user1 in range(0, len_rows):                        # Loop over each user
#     for user2 in range(0, len_rows):                    # And compare them with all users
#         l = []                                          # Create empty list
#         for column in range(0, len_columns):            # Loop over each state
#             # Compute the Euclidean distance
#             euclid = nan_euclidean_distances(train_data_matrix.values[user1, column].reshape(1, -1),
#                                              train_data_matrix.values[user2, column].reshape(1, -1))
#             # Compute the cosine similarity
#             #cosine = spatial.distance.cosine(train_data_matrix.values[user1, column].reshape(1, -1),
#             #                                 train_data_matrix.values[user2, column].reshape(1, -1))
#             if not math.isnan(euclid[0][0]):             # Append to list if not NaN
#                 l.append(float(euclid))
#         if l:                                            # If list is not empty take mean
#             dist = np.mean(l)
#         else:                                            # Else NaN
#             dist = np.nan
#         similarity_matrix[user1, user2] = dist           # Insert in similarity matrix
# # Save similarity matrix
# np.savetxt("matrix1.csv", similarity_matrix, delimiter=",")
file = open("matrix.csv")
similarity_matrix = np.loadtxt(file, delimiter=",")

# Compute sparsity level for the similarity matrix
total_count = np.prod(similarity_matrix.shape)
non_zero_count = np.isnan(similarity_matrix).sum()
matrix_sparsity = 1 - ((total_count - non_zero_count) / total_count)
print("The sparsity level for the similarity matrix is " + str(round(matrix_sparsity * 100, 3)) + "%")

print("You did it! You found the similarity between users. Now let's make some predictions")

# Predictions
# Find user with min distance
# Predict their sett. in new state

# Masked arrays
sim = similarity_matrix
num = train_data_matrix.iloc[:100, :292]
sim_masked = np.ma.array(sim, mask=np.isnan(sim))  # Convert to masked array with NumPy
num_masked = num.mask(num.isna())  # Convert to masked array with Pandas

# Calculation
denominator = np.ma.dot(sim_masked, num_masked)
numerator = (np.abs(sim_masked).sum(axis=1)).T
# Add small number to numerator to avoid division by 0
numerator = numerator + 1e-50

# Empty tensor with 3 dimensions
preds = np.zeros((np.shape(denominator)[0], np.shape(denominator)[1], 3))
# Loop to fill prediction tensor
for row in range(np.shape(denominator)[0]):
    for col in range(np.shape(denominator)[1]):
        for i in range(3):
            pred = denominator[row, col] / numerator[row]
            if type(pred) == np.float64:
                pass
            else:
                preds[row, col, i] = pred[i]

# Round values to closest mean
preds = np.round(preds)

# Does it stay within its intervals?
print(np.min(preds[:, :, 0]))
print(np.max(preds[:, :, 0]))
print(np.min(preds[:, :, 1]))
print(np.max(preds[:, :, 1]))
print(np.min(preds[:, :, 2]))
print(np.max(preds[:, :, 2]))

print("You did it! You found the predictions")

# https://medium.com/web-mining-is688-spring-2021/recommending-movies-to-users-using-distance-similarity-metrics-34179020290f


# num_masked = np.ma.array(num, mask=pd.isnull(num))   # Convert to masked array with NumPy
# numerator = np.abs(np.nansum(sim, axis=1)) + 1e-50  # Add small number to numerator to avoid division by 0


# Try
# denominator[9,32]
# denominator[9,32]/numerator[9] = close to 0 in all

# This line filters the NaN values by state
filter = [train_data_matrix[col][train_data_matrix[col].apply(lambda x: np.all(~np.isnan(x)))] for col in
          train_data_matrix.columns]

print("You did it! You found the predictions")
print(a)

df.round()
int(round(pred_matrix))
round(2.5, 1)

# sim = similarity_matrix - np.diag(np.diag(similarity_matrix))


# Take mean over columns (states)
# mean_user_sett = [train_data_matrix[col][train_data_matrix[col].apply(lambda x: np.all(~np.isnan(x)))].mean()
#                   for col in train_data_matrix.columns]
#
# # Subtract mean value from user-item matrix
# # Example test: train_data_matrix.iloc[26,0]-mean_user_sett[0]
# diff_matrix = train_data_matrix
# for row in range(0, np.shape(train_data_matrix)[0]):
#     for col in range(0, np.shape(train_data_matrix)[1]):
#         diff_matrix.iloc[row, col] += (- mean_user_sett[col])
#
# apply(lambda x: np.all(~np.isnan(x)))
# pred = mean_user_sett[:, np.newaxis] + similarity.dot(diff_matrix) / np.array([np.abs(similarity).sum(axis=1)])


# nan_euclidean_distances(train_data_matrix.values[1, 2].reshape(1, -1), train_data_matrix.values[2, 1].reshape(1, -1))
# sklearn.metrics.pairwise_distances(train_data_matrix, train_data_matrix, metric="cosine", force_all_finite=False)



#pairwise_distances(train_data_matrix.values[41, 4].reshape(1, -1),  train_data_matrix.values[11, 4].reshape(1, -1), metric='cosine')


# denominator = np.nan_to_num(similarity_matrix).dot(train_data_matrix.iloc[:100, :292].fillna(0))
# numerator = np.array([np.abs(np.nan_to_num(similarity_matrix)).sum(axis=1)])
# numerator = numerator + 1e-50   # Add small number to numerator to avoid division by 0
# numerator = numerator.T         # Transpose
# pred_matrix = denominator / numerator
#
# # Round values to closest int
# preds = np.round(pred_matrix[0])
#


#train_data_matrix.values[22, 20]
#train_data_matrix.values[42, 20]
#spatial.distance.cosine(train_data_matrix.values[22, 20].reshape(1, -1), train_data_matrix.values[42, 20].reshape(1, -1))
#ddist(train_data_matrix.values[22, 20].reshape(1, -1), train_data_matrix.values[42, 20].reshape(1, -1))
#pdist(train_data_matrix.values[22, 20].reshape(1, -1), train_data_matrix.values[42, 20].reshape(1, -1))
# cosine_similarity(train_data_matrix.values[22, 20].reshape(1, -1), train_data_matrix.values[42, 20].reshape(1, -1))
