from baseline import baseline_train, baseline_predict
import numpy as np

def Baseline1(data_matrix, ID_train,state_train,settings_train, ID_test, state_test, settings_test,space):

    """train and test model"""
    # train the baseline model on the training data
    trained_mean = baseline_train(data_matrix)

    #Set training accuracy to zero (it gets updated in the for loop)
    train_acc = 0
    # size of the train set
    n = np.size(ID_train)

    # collect errors
    train_errors = np.zeros((n, 3))
    for i in range(n):
        pred_setting = baseline_predict(state_train[i], trained_mean, space.states)
        error = (pred_setting - settings_train[i]) ** 2
        train_errors[i, :] = error
        if np.all(np.asarray(pred_setting) == np.asarray(settings_train[i])):
            train_acc += 1

    # calculate the mean error on the train set
    train_MSE = np.sqrt(np.mean(train_errors, 0))

    #Set test accuracy to zero
    test_acc = 0
    # size of test set
    n = np.size(ID_test)
    # collect errors
    test_errors = np.zeros((n, 3))
    for i in range(n):
        pred_setting = baseline_predict(state_test[i], trained_mean, space.states)

        # print("the predicted setting is ", pred_setting)
        # print("the true setting was ", test_settings[i])
        error = (pred_setting - settings_test[i]) ** 2
        test_errors[i, :] = error

        #Correct predicted setting
        if np.all(np.asarray(pred_setting) == np.asarray(settings_test[i])):
            test_acc += 1
    # calculate the mean error on the test set
    test_MSE = np.sqrt(np.mean(test_errors, 0))

    #print('train error B1 ', train_MSE)
    #print('test error B1 ', test_MSE)

    print('Done with B1')
    train_MSE = list(train_MSE)
    test_MSE = list(test_MSE)
    return train_MSE, test_MSE,train_acc,test_acc