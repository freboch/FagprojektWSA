import numpy as np                                                                                                                                             
import tensorly as tl                                                                                                                                         
from create_statespace_class import *                                                                                                                         
from data_class import *                                                                                                                                      
import numpy as np                                                                                                                                            
import pandas as pd                                                                                                                                           
import math                                                                                                                                                   
from sklearn.metrics.pairwise import pairwise_distances                                                                                                       
from sklearn.metrics import mean_squared_error                                                                                                                
from math import sqrt                                                                                                                                         
from sklearn.model_selection import train_test_split                                                                                                          
                                                                                                                                                              
np.random.seed(42)                                                                                                                                            
app = "EVOKE"                                                                                                                                                  
                                                                                                                                                              
# Import data and create lists for all 4 files and combine them                                                                                               
paths = [                                                                                                                                                      
    '/Users/AnneSophieHHansen/Desktop/part-00000-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5531-1-c000.json',                              
    '/Users/AnneSophieHHansen/Desktop/part-00001-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5532-1-c000.json',                              
    '/Users/AnneSophieHHansen/Desktop/part-00002-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5533-1-c000.json',                              
    '/Users/AnneSophieHHansen/Desktop/part-00003-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5534-1-c000.json']                              
                                                                                                                                                              
# Load data                                                                                                                                                   
space = Space(app)                                                                                                                                             
data = Data(paths, app)                                                                                                                                        
data.clean()                                                                                                                                                  
data.CF_clean()                                                                                                                                               
                                                                                                                                                              
                                                                                                                                                              
# Transform user ids (user_id), states (item_id) and settings (sett) to an array                                                                              
def trans_to_array(data):                                                                                                                                     
    user_id = np.asarray(data.IDs)                                                                                                                            
    item_id = np.asarray(data.states)                                                                                                                         
    sett = np.asarray(data.settings)                                                                                                                          
    return user_id, item_id, sett                                                                                                                             
                                                                                                                                                              
                                                                                                                                                              
user_id, item_id, sett = trans_to_array(train_set)                                                                                                               
user_id_test, item_id_test, sett_test = trans_to_array(test_set)                                                                                              
                                                                                                                                                              
                                                                                                                                                              
# Transform both train and test to dataframe                                                                                                                  
def trans_to_dataframe(user_id, item_id, sett):                                                                                                               
    # Make dataframe with all relevant variables                                                                                                              
    df = pd.DataFrame(list(zip(user_id, item_id, sett)), columns=["user_id", "item_id", "sett"])                                                              
    return df                                                                                                                                                 
                                                                                                                                                              
                                                                                                                                                              
df = trans_to_dataframe(user_id, item_id, sett)                                                                                                               
df_test = trans_to_dataframe(user_id_test, item_id_test, sett_test)                                                                                           
                                                                                                                                                              
                                                                                                                                                              
# Create the user-item matrix (train set only)                                                                                                                
def user_item_matrix(user_id, item_id, sett):                                                                                                                 
    # Make user-item matrix from df (train)                                                                                                                   
    train_data_matrix = df.pivot(index="user_id", columns="item_id", values="sett")                                                                           
                                                                                                                                                              
    return train_data_matrix                                                                                                                                  
                                                                                                                                                              
                                                                                                                                                              
train_data_matrix = user_item_matrix(user_id, item_id, sett)                                                                                                   
                                                                                                                                                              
                                                                                                                                                              
# Create the similarity matrix (train set only)                                                                                                               
def similarity_matrix(user_id, item_id, sett):                                                                                                                 
    # Loop over settings                                                                                                                                      
    l = len(sett)                                                                                                                                             
    # B                                                                                                                                                       
    sett1 = []                                                                                                                                                
    for i in range(l):                                                                                                                                        
        x = sett[i][0]                                                                                                                                        
        sett1.append(x)                                                                                                                                       
    # M                                                                                                                                                       
    sett2 = []                                                                                                                                                
    for i in range(l):                                                                                                                                        
        y = sett[i][1]                                                                                                                                        
        sett2.append(y)                                                                                                                                       
    # T                                                                                                                                                       
    sett3 = []                                                                                                                                                
    for i in range(l):                                                                                                                                        
        z = sett[i][2]                                                                                                                                        
        sett3.append(z)                                                                                                                                       
                                                                                                                                                              
    # Make dataframe with all relevant variables                                                                                                              
    df1 = pd.DataFrame(list(zip(user_id, item_id, sett1)), columns=["user_id", "item_id", "sett1"])                                                           
    df2 = pd.DataFrame(list(zip(user_id, item_id, sett2)), columns=["user_id", "item_id", "sett2"])                                                           
    df3 = pd.DataFrame(list(zip(user_id, item_id, sett3)), columns=["user_id", "item_id", "sett3"])                                                           
                                                                                                                                                              
    # Filter data                                                                                                                                             
    # Delete unique users (if user only appears once)                                                                                                         
    # df1 = df1[df1.duplicated(subset=['user_id'], keep=False)]                                                                                               
    # df2 = df2[df2.duplicated(subset=['user_id'], keep=False)]                                                                                               
    # df3 = df3[df3.duplicated(subset=['user_id'], keep=False)]                                                                                               
    # Delete duplicates (if a user has more than one setting for a state)                                                                                     
    # df1 = df1.drop_duplicates(subset=['user_id', 'item_id'], keep='last')                                                                                   
    # df2 = df2.drop_duplicates(subset=['user_id', 'item_id'], keep='last')                                                                                   
    # df3 = df3.drop_duplicates(subset=['user_id', 'item_id'], keep='last')                                                                                   
                                                                                                                                                              
    # Create user-item matrices                                                                                                                               
    nan = float("NaN")                                                                                                                                        
    train_data_matrix_1 = df1.pivot(index="user_id", columns="item_id", values="sett1")                                                                       
    train_data_matrix_2 = df2.pivot(index="user_id", columns="item_id", values="sett2")                                                                       
    train_data_matrix_3 = df3.pivot(index="user_id", columns="item_id", values="sett3")                                                                       
                                                                                                                                                              
    # Compute Euclidean distance                                                                                                                              
    from sklearn.metrics.pairwise import nan_euclidean_distances                                                                                              
    dist1 = nan_euclidean_distances(train_data_matrix_1.values)                                                                                               
    dist2 = nan_euclidean_distances(train_data_matrix_2.values)                                                                                               
    dist3 = nan_euclidean_distances(train_data_matrix_3.values)                                                                                               
                                                                                                                                                              
    # Take mean and thereby create one final similarity matrix                                                                                                
    sim_matrix = (dist1 + dist2 + dist3) / 3                                                                                                                  
                                                                                                                                                              
    return sim_matrix                                                                                                                                         
                                                                                                                                                              
                                                                                                                                                              
similarity_matrix = similarity_matrix(user_id, item_id, sett)                                                                                                 
                                                                                                                                                              
                                                                                                                                                              
# Save similarity matrix                                                                                                                                      
np.savetxt("sim_matrix_try.csv", similarity_matrix, delimiter=",")                                                                                            
                                                                                                                                                              
# Open saved similarity matrix                                                                                                                                
file = open("sim_matrix_try.csv")                                                                                                                              
similarity_matrix = np.loadtxt(file, delimiter=",")                                                                                                           
                                                                                                                                                              
                                                                                                                                                              
def backup_baseline():                                                                                                                                        
    # can be used to fill in for empty spots in the other baselines                                                                                           
    B = np.random.choice([-12, 12], 1)  # Can be either -12 or 12                                                                                             
    M = np.random.choice([-12, 6], 1)   # Can be either -12 or 6                                                                                              
    T = np.random.choice([-12, 6], 1)   # Can be either -12 or 6                                                                                              
    predict = np.asarray([B[0], M[0], T[0]])                                                                                                                  
    return predict                                                                                                                                            
                                                                                                                                                              
                                                                                                                                                              
# Make top 3 predictions for a specific user in a desired state                                                                                               
def prediction(user_id, item_id, similarity_matrix, user_item_matrix):                                                                                        
    # Find users index in similarity matrix (and user_item_matrix)                                                                                            
    index = np.nonzero(user_item_matrix.index == user_id)[0][0]                                                                                               
    # Sort its similarity measure                                                                                                                             
    sorted = np.sort(similarity_matrix[index, :])                                                                                                             
    # Find most similar users to the user in question                                                                                                         
    sorted_index = np.argsort(similarity_matrix[index, :])                                                                                                    
    # Assumption: Not necessary to remove the user itself                                                                                                     
    # Column for state of interest (item_id)                                                                                                                  
    state = user_item_matrix[item_id]                                                                                                                         
    # If most similar users have a sett. in the desired state (item_id) this will be the predicted sett.                                                      
    # Loop over most similar users                                                                                                                            
    pred_sett = []                                                                                                                                            
    for i in sorted_index:                                                                                                                                    
        # Append to list if prediction is not NaN and list is empty                                                                                           
        if type(state[i]) == np.ndarray and len(pred_sett) == 0:                                                                                              
            pred_sett.append(list(state[i]))                                                                                                                  
        # Append to list if prediction is not already in list                                                                                                 
        if type(state[i]) == np.ndarray and len(pred_sett) != 0:                                                                                              
            if not list(state[i]) in pred_sett:                                                                                                               
                pred_sett.append(list(state[i]))                                                                                                              
        # Stop if we have top 3 predictions                                                                                                                   
        if len(pred_sett) == 3:                                                                                                                               
            break                                                                                                                                             
                                                                                                                                                              
    # If there are not 3 predictions, append sett. from backup baseline (random corner)                                                                       
    if len(pred_sett) != 3:                                                                                                                                   
        for k in range(3 - len(pred_sett)):                                                                                                                   
            random = backup_baseline()                                                                                                                        
            random = list(random)                                                                                                                             
            pred_sett.append(random)                                                                                                                          
                                                                                                                                                              
    # Transform back to arrays                                                                                                                                
    pred_sett[0] = np.array(pred_sett[0])                                                                                                                     
    pred_sett[1] = np.array(pred_sett[1])                                                                                                                     
    pred_sett[2] = np.array(pred_sett[2])                                                                                                                     
                                                                                                                                                              
    return pred_sett                                                                                                                                          
                                                                                                                                                              
                                                                                                                                                              
pred_sett = prediction("0018b72738d7d06ebd8e854960fa613a072bac9e838ef292142a9268df482eca", "home_tv", similarity_matrix, train_data_matrix)                    
                                                                                                                                                              
print(pred_sett)                                                                                                                                              
                                                                                                                                                              
print("Stoooop")                                                                                                                                              
                                                                                                                                                              
# 0018b72738d7d06ebd8e854960fa613a072bac9e838ef292142a9268df482eca                                                                                            
# 000b9549bcc02e4769ea17870b58afb8469f43dbb09ea3e4672ef2dbd8529586                                                                                            
                                                                                                                                                              







