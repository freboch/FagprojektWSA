
from create_statespace_class import Space
import numpy as np
from data_class import *
import pandas as pd
from transform_functions import *
from nmf import *
from scipy import linalg


app = "EVOKE"
# import data and create lists for all 4 files and combine them
paths = ['part-00000-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5531-1-c000.json',
         'part-00001-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5532-1-c000.json',
         'part-00002-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5533-1-c000.json',
         'part-00003-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5534-1-c000.json']

space = Space(app)
data = Data(paths,app)
data.clean()
settings = ['Bass','Midrange','Trebble']

def Unique(data_IDs,data_states):
    ID_ar = np.asarray(data_IDs)
    st_ar = np.asarray(data_states)
    U_ID = list(np.unique(ID_ar))
    U_states = list(np.unique(st_ar))
    #U_ID = set(list(data_IDs))
    #U_states = set(list(data_states))

    return U_ID, U_states
U_ID,U_states = Unique(data.IDs,data.states)

df = pd.read_pickle('User_Item_positive.pkl')
s_B, s_M,s_T = np.empty((len(U_ID),len(U_states))),np.empty((len(U_ID),len(U_states))),\
               np.empty((len(U_ID),len(U_states)))
def create_matrices(df,U_ID,U_states,s_B,s_M,s_T):
    q=0
    for i, id in enumerate(U_ID):
        for j,state in enumerate(U_states):
            f = df.loc[id, state]
            if type(f) == float:
                pass
            else:
                s_B[i,j] =f[0]
                s_M[i,j] = f[1]
                s_T[i,j] = f[2]
                q+=3
        print('round {} of {}'.format(i+1, len(U_ID)))
    return s_B,s_M,s_T,q

mat_B, mat_M,mat_T,q = create_matrices(df,U_ID,U_states,s_B,s_M,s_T)

matrix = np.asarray(np.hstack([mat_B,mat_M,mat_T]))

W,H,info = NMF_HALS().run(matrix, 3, max_iter=1000)

fac_mat = np.dot(W,H.T)
fac_mat = np.round(fac_mat)
p_B,p_M,p_T = fac_mat[:,:len(U_states)], fac_mat[:,len(U_states):2*len(U_states)], \
              fac_mat[:,2*len(U_states):]

W_B,H_B,info_B = NMF_HALS().run(mat_B, 3, max_iter=1000)
fac_mat_B = np.dot(W_B,H_B.T)
fac_mat_B = np.round(fac_mat_B)


print(a)