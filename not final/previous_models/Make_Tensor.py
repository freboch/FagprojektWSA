from create_statespace_class import Space
from data_class import Data
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split

from sklearn.metrics.pairwise import pairwise_distances
from sklearn.metrics import mean_squared_error
from math import sqrt

np.random.seed(42)
app = "EVOKE"

# Create statespace based on chosen app
#environments, intents, states = create_statespace(app)

# Import data and create lists for all 4 files and combine them
paths = ['part-00000-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5531-1-c000.json',
         'part-00001-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5532-1-c000.json',
         'part-00002-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5533-1-c000.json',
         'part-00003-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5534-1-c000.json']

space = Space(app)
data = Data(paths,app)
def trans(data):
    #To array
    data_IDs = np.asarray(data.IDs)
    data_states = np.asarray(data.states)
    data_settings = np.asarray(data.settings)

    return data_IDs, data_states, data_settings

data_IDs, data_states, data_settings = trans(data)