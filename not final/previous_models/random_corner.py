import numpy as np

"""Function that randomly chooses between the corner settings. """

def random_corner():

    B = np.random.choice([-12, 12])     # Can be either -12 or 14
    M = np.random.choice([-12, 6])      # Can be either -12 or 8
    T = np.random.choice([-12, 6])      # Can be either -12 or 8
    pred = [B, M, T]

    return pred
