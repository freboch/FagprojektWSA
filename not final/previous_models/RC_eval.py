import numpy as np

"""Function that randomly chooses between the corner settings. """

def random_corner(ID_train,ID_test, settings_train,settings_test):

    preds_train = []
    preds_test = []

    #Train accuracy
    train_acc = 0
    n = np.size(ID_train)
    train_errors = []

    # Make three corner predictions per new user training
    for i in range(n):

        B = np.random.choice([-12, 12],3)     # Can be either -12 or 14
        M = np.random.choice([-12, 6],3)      # Can be either -12 or 8
        T = np.random.choice([-12, 6],3)      # Can be either -12 or 8
        predict = [[B[0], M[0], T[0]],[B[1], M[1], T[1]],[B[2], M[2], T[2]]]
        preds_train.append(predict)

        #Calculate accuracy
        if np.all(np.asarray(predict[0]) == np.asarray(settings_train[i])):
            train_acc += 1
        elif np.all(np.asarray(predict[1]) == np.asarray(settings_train[i])):
            train_acc += 1
        elif np.all(np.asarray(predict[2]) == np.asarray(settings_train[i])):
            train_acc += 1

        #Calculate RMSE for train
        tr_errors = [list((predict[0]-settings_train[i])**2),list((predict[1]-settings_train[i])**2),
                               list((predict[2]-settings_train[i])**2)]

        min_list = min(tr_errors,key = sum)

        train_errors.append(min_list)
    train_eB2 = np.sqrt(np.mean(train_errors, 0))

    test_acc = 0
    test_errors = []
    n1 = np.size(ID_test)
    #Make three corner predictions per new user test

    for i in range(n1):
        B1 = np.random.choice([-12, 12],3)     # Can be either -12 or 12
        M1 = np.random.choice([-12, 6],3)      # Can be either -12 or 6
        T1 = np.random.choice([-12, 6],3)      # Can be either -12 or 6
        predict1 = [[B1[0],M1[0],T1[0]],[B1[1],M1[1],T1[1]],[B1[2],M1[2],T1[2]]]
        preds_test.append(predict1)

        #Calculate accuracy
        if np.all(np.asarray(predict1[0]) == np.asarray(settings_test[i])):
            test_acc += 1
        elif np.all(np.asarray(predict1[1]) == np.asarray(settings_test[i])):
            test_acc += 1
        elif np.all(np.asarray(predict1[2]) == np.asarray(settings_test[i])):
            test_acc += 1

        #Calculate RMSE for test data
        te_errors = [list((predict1[0] - settings_test[i]) ** 2), list((predict1[1] - settings_test[i]) ** 2),
                     list((predict1[2] - settings_test[i]) ** 2)]

        min_list_te = min(te_errors, key=sum)

        test_errors.append(min_list_te)

    test_eB2 = np.sqrt(np.mean(test_errors, 0))

    print('Done with B2')
    train_eB2 = list(train_eB2)
    test_eB2 = list(test_eB2)
    return train_eB2, test_eB2,train_acc, test_acc
