import numpy as np
import math

def baseline_train(data_matrix):
    """
    :param train_states: list of strings
    :param train_IDs: list of strings
    :param train_settings: list of lists: each setting is a list with 3 integers
    :return: state x 3 matrix with the median sound setting for each state
    """
    # for each state compute the median Bass, midrange and treble setting
    means = np.nanmean(data_matrix, axis=1)
    total_mean = np.nanmean(data_matrix)
    # if a state is not represented in the training Data it gets the total median value assigned
    for i, val in enumerate(means[0, :]):
        if math.isnan(val):
            means[0, i] = total_mean
            means[1, i] = total_mean
            means[2, i] = total_mean
    # The predictions should be integer like the sound settings
    means = np.floor(means)
    means = means.astype(int)
    return means


def baseline_predict(test_state, trained_mean, states):
    """
    :param test_state: string with state
    :param trained_mean: output from the baseline training function. state x setting matrix
    :return: setting corresponding to the input state: list length 3 with integers
    """
    idx_st = states.index(test_state)
    setting = trained_mean[:, idx_st]
    return setting
