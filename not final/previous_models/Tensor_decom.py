import tensorly as tl
from create_statespace_class import Space
from data_class import *
import pandas as pd
from nmf import *


app = "EVOKE"
# import data and create lists for all 4 files and combine them
paths = ['part-00000-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5531-1-c000.json',
         'part-00001-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5532-1-c000.json',
         'part-00002-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5533-1-c000.json',
         'part-00003-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5534-1-c000.json']

space = Space(app)
data = Data(paths,app)
data.clean()
settings = ['Bass','Midrange','Trebble']

def Unique(data_IDs,data_states):
    U_ID = set(list(data_IDs))
    U_states = set(list(data_states))

    return U_ID, U_states
U_ID,U_states = Unique(data.IDs,data.states)

df = pd.read_pickle('User_Item_positive.pkl')
tens = tl.tensor(np.empty((len(U_ID),len(U_states),len(settings))))
tens1 = tl.tensor(np.zeros((len(U_ID),len(U_states),len(settings))))
t = np.zeros((len(U_ID),len(U_states),len(settings)))
#print(a)
def create_tensor(df,U_ID,U_states,tens,t,tens1):

    for i, id in enumerate(U_ID):
        for j,state in enumerate(U_states):
            f = df.loc[id, state]
            if type(f) == float:

                tens1[i, j, 0] = 30
                tens1[i, j, 1] = 30
                tens1[i, j, 2] = 30
                pass
            else:
                tens[i,j,0] = f[0]
                tens[i,j,1] = f[1]
                tens[i,j,2] = f[2]
                tens1[i, j, 0] = f[0]
                tens1[i, j, 1] = f[1]
                tens1[i, j, 2] = f[2]
                t[i,j,0] = 1
                t[i,j,1] = 1
                t[i,j,2] = 1
        print('round {} of {}'.format(i, len(U_ID)))
    return tens,t, tens1
#tens_filled, t_01,tens1_filled = create_tensor(df,U_ID,U_states,tens,t,tens1)

s_B, s_M,s_T = np.empty((len(U_ID),len(U_states)))*np.nan,np.empty((len(U_ID),len(U_states)))*np.nan,\
               np.empty((len(U_ID),len(U_states)))*np.nan

def create_matrices(df,U_ID,U_states,s_B,s_M,s_T):
    q=0
    for i, id in enumerate(U_ID):
        for j,state in enumerate(U_states):
            f = df.loc[id, state]
            if type(f) == float:
                pass
            else:
                s_B[i,j] =f[0].astype(int)
                s_M[i,j] = f[1].astype(int)
                s_T[i,j] = f[2].astype(int)
                q+=3
        print('round {} of {}'.format(i+1, len(U_ID)))
    return s_B,s_M,s_T,q

mat_B, mat_M,mat_T,q = create_matrices(df,U_ID,U_states,s_B,s_M,s_T)

matrix = np.asarray(np.bmat([mat_B,mat_M,mat_T]))
sparsity = q/(len(matrix[:,0])*len(matrix[0,:]))

print('sparsity is {}'.format(sparsity))

print(a)



