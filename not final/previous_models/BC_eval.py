from data_class import *

from collections import Counter
import numpy as np

# Driver data - remove later
app = "EVOKE"
paths = ['part-00000-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5531-1-c000.json',
         'part-00001-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5532-1-c000.json',
         'part-00002-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5533-1-c000.json',
         'part-00003-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5534-1-c000.json']

data = Data(paths, app)

"""Function that chooses the most chosen corner setting for that environment."""

env1 = ['car', 'cinema', 'classroom', 'home', 'largeHall', 'nature', 'noisyVenue', 'other', 'restaurant', 'transport', 'urban', 'work']

# Runs one env

def best_corner_simple(env):
    set = []
    for i, j in enumerate(data.envs):
        if env == j:
            set.append(tuple(data.settings[i]))
    count = Counter(set)
    pred = list(count.most_common(1)[0][0])
    return type(pred)

# Runs a list of env

def best_corner_dict(all_env, env_train, settings_train):
    dic = {}
    for v in all_env:
        set = []
        for i, j in enumerate(env_train):
            if v == j:
                set.append(tuple(settings_train[i]))
        count = Counter(set)
        print(count)
        pred = count.most_common(1)[0][0]
        dic[v] = pred
    print(dic)
    return dic

def best_corner_eval(dic, env_train, env_test, settings_train, settings_test):

    preds_train = []
    preds_test = []

    for i in env_train:
        preds_train.append(dic[i])

    for j in env_test:
        preds_test.append(dic[j])

    train_errors = (preds_train - settings_train) ** 2
    train_eB3 = np.sqrt(np.mean(train_errors, 0))

    test_errors = (preds_test - settings_test) ** 2
    test_eB3 = np.sqrt(np.mean(test_errors, 0))

    return train_eB3, test_eB3








