from collections import Counter
import numpy as np

def best_corner_dict(all_states, state_train, settings_train):
    dic = {}
    for state in all_states:
        set = []
        for i,j in enumerate(state_train):
            if state == j:
                set.append(tuple(settings_train[i]))
        occurence_count = Counter(set)
        dic[state] = list(occurence_count.most_common(1))

    return dic

def best_corner_eval(dic, state_train, state_test, settings_train, settings_test):
    preds_train = []
    preds_test = []
    q = 0
    train_acc = 0

    for i in state_train:
        preds_train.append(dic[i][0][0])
        if np.all(np.asarray(dic[i][0][0]) == np.asarray(settings_train[q])):
            train_acc += 1
        q+=1

    z = 0
    test_acc = 0
    for i in state_test:
        if dic[i] == []:
            B = np.random.choice([-12, 12])  # Can be either -12 or 14
            M = np.random.choice([-12, 6])  # Can be either -12 or 8
            T = np.random.choice([-12, 6])  # Can be either -12 or 8
            pred = [B, M, T]
            preds_test.append(pred)

            if np.all(np.asarray(pred) == np.asarray(settings_test[z])):
                test_acc += 1
        else:
            preds_test.append(dic[i][0][0])

            if np.all(np.asarray(dic[i][0][0]) == np.asarray(settings_test[z])):
                test_acc += 1
        z += 1


    train_errors = (preds_train - settings_train) ** 2
    train_eB3 = np.sqrt(np.mean(train_errors, 0))

    test_errors = (preds_test - settings_test) ** 2
    test_eB3 = np.sqrt(np.mean(test_errors, 0))


    train_eB3 = list(train_eB3)
    test_eB3 = list(test_eB3)
    print('Done with B3')
    return train_eB3, test_eB3,train_acc,test_acc


