from collections import Counter
import numpy as np

def best_corner_dict(all_states, state_train, settings_train):
    # Make empty dictionary to coontain the states and 3 most used settings
    dic = {}
    # Loop over all states
    for state in all_states:
        set = []
        for i,j in enumerate(state_train):
            if state == j:
                # append the setting to the state
                set.append(tuple(settings_train[i]))
        occurence_count = Counter(set)
        # Make the dictionary consist of the three most used settings in the given state
        dic[state] = list(occurence_count.most_common(3))

    return dic

def best_corner_eval(dic, state_train, state_test, settings_train, settings_test):
    preds_train = []
    preds_test = []
    train_acc = 0

    # Make predictions for the training data
    for i,state in enumerate(state_train):
        dic_preds = []
        # Not all states have an equal number of settings, so we loop through them
        for j in range(len(dic[state])):
            # Add the setting to the predictions
            dic_preds.append(dic[state][j][0])
            # Calculate accuracy i.e. check if the prediction matches the setting from the train set
            if np.all(np.asarray(dic[state][j][0]) == np.asarray(settings_train[i])):
                train_acc += 1
        preds_train.append(dic_preds)

    test_acc = 0
    for i,state in enumerate(state_test):
        # In case we encount a state, which do not have a setting

        if dic[state] == []:
            # Make 3 predictions
            B = np.random.choice([-12, 12], 3)  # Can be either -12 or 14
            M = np.random.choice([-12, 6], 3)  # Can be either -12 or 8
            T = np.random.choice([-12, 6], 3)  # Can be either -12 or 8
            predict = [[B[0], M[0], T[0]], [B[1], M[1], T[1]], [B[2], M[2], T[2]]]
            preds_test.append(predict)

            # Calculate the accuracy
            if np.all(np.asarray(predict[0]) == np.asarray(settings_test[i])):
                train_acc += 1
            elif np.all(np.asarray(predict[1]) == np.asarray(settings_test[i])):
                train_acc += 1
            elif np.all(np.asarray(predict[2]) == np.asarray(settings_test[i])):
                train_acc += 1
        else:
        # If the state is is the training set
            predict = []
            for j in range(len(dic[state])):
                predict.append(dic[state][j][0])
                # Calculate accuracy
                if np.all(np.asarray(dic[state][j][0]) == np.asarray(settings_test[i])):
                    test_acc += 1
            preds_test.append(predict)
    # Calculate RMSE error for training data
    train_errors = []
    for i in range(len(settings_train)):
        tr_error = []
        for j in range(len(preds_train[i])):
            tr_error.append(list((preds_train[i][j]-settings_train[i])**2))

        min_list = min(tr_error,key = sum)
        train_errors.append(min_list)

    train_eB3 = np.sqrt(np.mean(train_errors, 0))

    # Calculate RMSE error for test data
    test_errors = []
    for i in range(len(settings_test)):
        te_error = []
        for j in range(len(preds_test[i])):
            te_error.append(list((preds_test[i][j]-settings_test[i])**2))

        min_list_te = min(te_error,key = sum)
        test_errors.append(min_list_te)

    test_eB3 = np.sqrt(np.mean(test_errors, 0))


    train_eB3 = list(train_eB3)
    test_eB3 = list(test_eB3)
    print('Done with B3')
    return train_eB3, test_eB3,train_acc,test_acc


