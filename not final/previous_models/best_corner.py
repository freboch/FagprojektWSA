from data_class import *
from collections import Counter
import numpy as np

# Driver data - remove later
app = "EVOKE"
paths = ['part-00000-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5531-1-c000.json',
         'part-00001-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5532-1-c000.json',
         'part-00002-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5533-1-c000.json',
         'part-00003-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5534-1-c000.json']

data = Data(paths, app)

"""Function that chooses the most chosen corner setting for that environment."""

env1 = ['car', 'cinema', 'classroom', 'home', 'largeHall', 'nature', 'noisyVenue', 'other', 'restaurant', 'transport', 'urban', 'work']
#env2 = ['car', 'cinema', 'classroom', 'home', 'largeHall', 'nature']

# Runs one env

def best_corner_simple(env):
    set = []
    for i, j in enumerate(data.envs):
        if env == j:
            set.append(tuple(data.settings[i]))
    count = Counter(set)
    pred = list(count.most_common(1)[0][0])
    return type(pred)

# Runs a list of env

def best_corner_train(env):
    best = []
    for v in env:
        set = []
        for i, j in enumerate(data.envs):
            if v == j:
                set.append(tuple(data.settings[i]))
        count = Counter(set)
        pred = list(count.most_common(1)[0][0])
        best.append(pred)
    dic = dict(zip(env,best))
    return dic


dic = best_corner_train(env1)

def best_corner_test(env, dic):
    preds = []
    for j in env:
        preds.append(dic[j])
    return preds

preds = best_corner_test(env1, dic)
print(preds)







