from create_statespace_class import Space
from data_class import Data
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from transform_functions import *
import tensorly as tl

np.random.seed(42)
app = "EVOKE"

# Create statespace based on chosen app
#environments, intents, states = create_statespace(app)

# Import data and create lists for all 4 files and combine them
paths = ['part-00000-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5531-1-c000.json',
         'part-00001-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5532-1-c000.json',
         'part-00002-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5533-1-c000.json',
         'part-00003-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5534-1-c000.json']

space = Space(app)
data = Data(paths,app)
data.clean()
def trans(data):
    #To array
    data_IDs = np.asarray(data.IDs)
    data_states = np.asarray(data.states)

    data_settings = np.asarray(data.settings)

    return data_IDs, data_states, data_settings

data_IDs, data_states, data_settings = trans(data)

#Identify unique ID's
def UI_dataframe(data_IDs,data_states):
    U_ID = set(list(data_IDs))
    U_states = set(list(data_states))
    ID_ar = np.asarray(data_IDs)
    st_ar = np.asarray(data_states)
    U_ID = list(np.unique(ID_ar))
    U_states = list(np.unique(st_ar))

    df = pd.DataFrame(index=U_states, columns=U_ID)
    df.fillna(0)

    return df, U_states,U_ID
df,U_states,U_ID = UI_dataframe(data_IDs,data_states)

def Create_UI_matrix(df,data_IDs,data_states,data_settings):
    U_ID_ar = df.columns
    j= 0
    q= 1
    for i in U_ID_ar:
        Index = np.array(np.where(data_IDs == i))
        for j in range(np.size(Index)):
            k = data_states[Index[:,j]]
            df.at[k[0],i] = (data_settings[Index[:,j]][0])
            j+=1
        print('round {} of {}'.format(q,len(U_ID_ar)))
        q += 1
    df = df.T
    df.to_pickle(r'User_Item_df.pkl')
    return df

df = Create_UI_matrix(df,data_IDs,data_states,data_settings)

def create_test_train_matrix(df):
    train_data, test_data = train_test_split(df,test_size=0.2)
    train_data.to_csv(r'User_Item_train.csv',index=True,index_label='State',header=True)
    test_data.to_csv(r'User_Item_test.csv',index=True,index_label='State',header=True)


#create_test_train_matrix(df,U_ID,U_states)
def df_nn(df,U_ID,U_states):
    for i,id in enumerate(U_ID):
        for j,state in enumerate(U_states):
            if type(df.loc[id,state]) == float:
                pass
            else:
                df.loc[id,state] = transform_data('positive', df.loc[id, state])
        print('almost done {} of {}'.format(i+1,len(U_ID)))
    df.to_pickle(r'User_Item_positive.pkl')

df_nn(df,U_ID,U_states)
