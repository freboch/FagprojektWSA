import numpy as np


def create_matrix(train_states, train_IDs, train_settings,states):
    """
    :param train_states: list of strings
    :param train_IDs: list of strings
    :param train_settings: list of lists: each setting is a list with 3 integers
    :return: state x 3 matrix with the median sound setting for each state
    """
    # find the unique IDs
    IDs = list(set(train_IDs))
    # create the datamatrix with the training Data user IDs and the total state space
    data_matrix = np.ones((3, len(IDs), len(states)))
    data_matrix[:] = None
    for i, state in enumerate(train_states):
        idx0 = IDs.index(train_IDs[i])
        idx1 = states.index(state)
        # insert the setting in the Data matrix
        data_matrix[:, idx0, idx1] = train_settings[i]
    return data_matrix