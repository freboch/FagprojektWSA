import numpy as np
import json


# create class
class Data:
    def __init__(self, file_paths, app):
        # load Data from list of paths
        data_list = []
        for path in file_paths:
            data_list.extend([json.loads(line) for line in open(path, 'r')])

        """ split the Data list into lists with states, IDs and settings etc. of the chosen app"""
        # initiate lists
        no_data = []
        data_IDs = []
        data_states = []
        data_settings = []
        data_envs = []
        data_intents = []
        data_progress = []
        data_duration = []
        data_usage = []
        data_matrix_m = []
        data_matrix_x = []
        data_vector_y = []
        for i, line in enumerate(data_list):
            # one line does not have an app_ID_hash, it is removed from the Data
            if 'app_id_hash' not in line:
                no_data.append(i)
                continue
            # if we want to only look at a specific hearing aid:
            if (app == 'EVOKE' and line['ha_family'] != 23) or (app == 'MOMENT' and line['ha_family'] != 28):
                no_data.append(i)
                continue
            # ID
            ID = line['app_id_hash']
            data_IDs.append(ID)
            # Setting
            setting = np.asarray(json.loads(line['learned_best_setting']))
            data_settings.append(setting)
            # state
            tag_dict = json.loads(line['tags'])  # dictionary with the tags
            env = tag_dict['outer']  # string with env tag
            intent = tag_dict['inner']  # list with 0, 1 or 2 strings with intent tag
            # create the env and intent list
            data_envs.append(env)
            # create the state tag for the line
            if len(intent) == 0:
                state = env
                data_intents.append('None')
            elif len(intent) == 1:
                state = env + '_' + intent[0]
                data_intents.append(intent[0])
            else:  # len(intent) == 2:
                intent.sort()  # sort alphabetically so the state name will match the one in the states list
                state = env + '_' + intent[0] + '_' + intent[1]
                data_intents.append(intent[0] + '_' + intent[1])
            data_states.append(state)

            # progress
            progress = line['progress']
            data_progress.append(progress)
            # duration days
            duration = line['duration_days']
            data_duration.append(duration)
            # usage count
            usage = line['usage_count']
            data_usage.append(usage)

            # matrix x
            mat_x = line['matrix_x']
            data_matrix_x.append(mat_x)
            # matrix m
            mat_m = line['matrix_m']
            data_matrix_m.append(mat_m)
            # matrix x
            vec_y = line['vector_y']
            data_vector_y.append(vec_y)

        self.IDs = data_IDs
        self.states = data_states
        self.settings = data_settings
        self.envs = data_envs
        self.intents = data_intents
        self.progress = data_progress
        self.duration = data_duration
        self.usage = data_usage
        self.matrix_x = data_matrix_x
        self.matrix_m = data_matrix_m
        self.vector_y = data_vector_y

        self.N = len(self.IDs)

        unique_IDs, ID_counts = np.unique(np.asarray(self.IDs), return_counts=True)
        self.unique_IDs = unique_IDs
        self.ID_counts = ID_counts

    def remove_observation(self, observation_index):
        # observation_index is a list of indices to be removed from the dataset
        for index in sorted(observation_index, reverse=True):
            del self.IDs[index]
            del self.states[index]
            del self.settings[index]
            del self.envs[index]
            del self.intents[index]
            del self.progress[index]
            del self.duration[index]
            del self.usage[index]
            del self.matrix_x[index]
            del self.matrix_m[index]
            del self.vector_y[index]

        unique_IDs, ID_counts = np.unique(np.asarray(self.IDs), return_counts=True)
        self.unique_IDs = unique_IDs
        self.ID_counts = ID_counts
        self.N = len(self.IDs)

    def clean(self):
        remove = []
        for i in range(self.N):
            # Progress
            if self.progress[i] < 30:
                remove.append(i)
            # Duration
            # duration longer than 3 years remove
            elif self.duration[i] > 365 * 3:
                remove.append(i)
            # usage count
            elif self.usage[i] < 3:
                remove.append(i)
            # Number of programs per unique ID: 20 limit
            elif self.ID_counts[np.where(self.unique_IDs == self.IDs[i])] > 20:
                remove.append(i)
            # demo-behaviour in y-vector
            elif np.all(np.asarray(self.vector_y[i]) == 0.5):
                remove.append(i)
            # remove environment = other and intent = other
            elif self.envs[i] == 'other' or 'other' in self.intents[i].split("_"):
                remove.append(i)
        self.remove_observation(remove)

    def useful_states(self, threshold):
        state_titles, state_counts = np.unique(np.asarray(self.states), return_counts=True)
        remove_states = [state_titles[i] for i, count in enumerate(state_counts) if count < threshold]
        remove = []
        for i in range(self.N):
            if self.states[i] in remove_states:
                remove.append(i)

        self.remove_observation(remove)
