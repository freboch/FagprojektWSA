import numpy as np
import json
from tqdm import tqdm


# create class
class Data:
    def __init__(self, file_paths, app):
        # load Data from list of paths
        data_list = []
        for path in file_paths:
            data_list.extend([json.loads(line) for line in open(path, 'r')])

        """ split the Data list into lists with states, IDs and settings etc. of the chosen app"""
        # initiate lists
        no_data = []
        data_IDs = []
        data_states = []
        data_settings = []
        data_envs = []
        data_intents = []
        intent_list = []
        data_progress = []
        data_duration = []
        data_usage = []
        data_matrix_m = []
        data_matrix_x = []
        data_vector_y = []
        
        ID_num = 0

        print("Creating data class object with {} observations".format(len(data_list)))
        for i, line in enumerate(data_list):
            # one line does not have an app_ID_hash, it is removed from the Data
            if 'app_id_hash' not in line:
                no_data.append(i)
                continue
            # if we want to only look at a specific hearing aid:
            if (app == 'EVOKE' and line['ha_family'] != 23) or (app == 'MOMENT' and line['ha_family'] != 28):
                no_data.append(i)
                continue
            # ID
            ID = line['app_id_hash']
            data_IDs.append(ID)
            # Setting
            setting = np.asarray(json.loads(line['learned_best_setting']))
            data_settings.append(setting)
            # state
            tag_dict = json.loads(line['tags'])  # dictionary with the tags
            env = tag_dict['outer']  # string with env tag
            intent = tag_dict['inner']  # list with 0, 1 or 2 strings with intent tag
            # add to environment list
            data_envs.append(env)
            # create the intent and state tag for the line
            if len(intent) == 0:
                state = env
                data_intents.append('None')
                intent_list.append(['None'])
            elif len(intent) == 1:
                state = env + '_' + intent[0]
                data_intents.append(intent[0])
                intent_list.append(intent)
            elif len(intent) == 2:
                intent.sort()  # sort alphabetically so the state name will match the one in the states list
                state = env + '_' + intent[0] + '_' + intent[1]
                data_intents.append(intent[0] + '_' + intent[1])
                intent_list.append(intent)
            data_states.append(state)

            # progress
            progress = line['progress']
            data_progress.append(progress)
            # duration days
            duration = line['duration_days']
            data_duration.append(duration)
            # usage count
            usage = line['usage_count']
            data_usage.append(usage)

            # matrix x
            mat_x = line['matrix_x']
            x = mat_x.split(';')
            n = len(x)
            new_x = np.empty((n, 3))
            for k, row in enumerate(x):
                individual = row.split()
                for j, digit in enumerate(individual):
                    new_x[k, j] = float(digit)
            data_matrix_x.append(new_x)

            # vector y
            vec_y = line['vector_y']
            m = len(vec_y)
            data_vector_y.append(vec_y)
            
            # matrix m
            mat_m = line['matrix_m']
            mat = mat_m.split(';')
            new_m = np.empty((m, n))
            for k in range(m):
                row = mat[k]
                individual = row.split()
                for j, digit in enumerate(individual):
                    new_m[k, j] = float(digit)
            data_matrix_m.append(new_m)

        # ID for each line (string)
        self.IDs = data_IDs
        
        # state for each line (string)       
        self.states = data_states
        # setting for each line (list)
        self.settings = data_settings
        # environment for each line (string)
        self.envs = data_envs
        # intent for each line (string) 
        self.intents = data_intents
        # list with intents (list)
        self.intent_list = intent_list
        
        # progress for each line (float)
        self.progress = data_progress
        # duration days count for each line (float)
        self.duration = data_duration
        # usage count for each line (float)
        self.usage = data_usage
        
        # nx3 matrix for each line
        self.matrix_x = data_matrix_x
        # nxm matrix for each line
        self.matrix_m = data_matrix_m
        # m-length vector for each line
        self.vector_y = data_vector_y
        
        ID_titles, ID_counts = np.unique(np.asarray(self.IDs), return_counts=True)
        
        # total number of lines/saved programs
        self.N = len(self.IDs)
        # all unique IDs
        self.unique_IDs = list(ID_titles)
        # number of programs per unique ID
        self.ID_counts = list(ID_counts)
        
        # all unique IDs in integer
        unique_IDs_int = [i for i in range(len(ID_titles))]
        self.unique_IDs_int = unique_IDs_int
        
        # ID as integer for each line (int)
        IDs_int = [self.unique_IDs.index(idt) for idt in self.IDs]
        self.IDs_int = IDs_int

        self.index = [i for i in range(self.N)]

    def remove_observation(self, observation_index):
        """
        Removes chosen observations from the data class
        observation_index is a sorted list of indices to be removed from the dataset.
        """
        
        for index in sorted(observation_index, reverse=True):
            del self.IDs[index]
            del self.IDs_int[index]
            
            del self.states[index]
            del self.settings[index]
            del self.envs[index]
            del self.intents[index]
            del self.intent_list[index]
            
            del self.progress[index]
            del self.duration[index]
            del self.usage[index]
            
            del self.matrix_x[index]
            del self.matrix_m[index]
            del self.vector_y[index]
            
        ID_titles, ID_counts = np.unique(np.asarray(self.IDs), return_counts=True)
        
        self.N = len(self.IDs)
        self.unique_IDs = list(ID_titles)
        self.unique_IDs_int = list(set(self.IDs_int))
        self.ID_counts = list(ID_counts)
        self.index = [i for i in range(self.N)]

    def clean(self):
        """ 
        Filters the dataset so only the useful observations remain in the data class
        """
        print("Cleaning data class object with {} observations".format(self.N))
        start_N = self.N
        # remove all IDs that have more than 20 programs
        ID_remove = [self.unique_IDs[i] for i, count in enumerate(self.ID_counts) if count > 20]
        remove = [i for i, ids in enumerate(self.IDs) if ids in ID_remove]
        self.remove_observation(remove)

        # find all the observations that should be removed based on observation-specific criteria
        remove = []
        for i in tqdm(range(self.N)):
            # Progress under 30
            if self.progress[i] < 30:
                remove.append(i)
            # duration longer than 2 years remove
            elif self.duration[i] > 365 * 2:
                remove.append(i)
            # usage count less than 3
            elif self.usage[i] < 3:
                remove.append(i)
            # demo-behaviour in y-vector
            elif np.all(np.asarray(self.vector_y[i]) == 0.5):
                remove.append(i)
            # if environment is "other" or intent is "other" or 'None'
            elif self.envs[i] == 'other':
                remove.append(i)
            # if there are 2 intents and one is "other" just remove the "other"
            elif len(self.intent_list[i]) == 2 and np.any(np.asarray(self.intent_list[i]) == 'other'):
                # fix intent_list
                self.intent_list[i].remove('other')
                # fix intent string
                int_txt = self.intents[i]
                int_x = int_txt.split("_")
                int_x.remove('other')
                self.intents[i] = int_x[0]
                # fix state string
                state_txt = self.states[i]
                state_x = state_txt.split("_")
                state_x.remove('other')
                self.intents[i] = state_x[0] + '_' + state_x[1]
            # if there is only one intention and it is 'other' or 'None'
            elif len(self.intent_list[i]) == 1 and (self.intents[i] == 'other' or self.intents[i] == 'None'):
                remove.append(i)
                
        # remove the found observations
        self.remove_observation(remove)

        # remove all programs that are the only program in their state
        self.useful_states(1)
        end_N = self.N
        print("Finished cleaning. Data class object now has {} observations".format(end_N))
        

    def useful_states(self, threshold):
        """
        if a state has fewer than or equal to threshold settings,
        all programs in that state will be removed from the data class
        """ 
        state_titles, state_counts = np.unique(np.asarray(self.states), return_counts=True)
        remove_states = [state_titles[i] for i, count in enumerate(state_counts) if count <= threshold]
        remove_IDX = [i for i, state in enumerate(self.states) if state in remove_states]

        self.remove_observation(remove_IDX)


    def CF_clean(self):
        # remove all IDs that have only 1 program
        ID_remove = [self.unique_IDs[i] for i, count in enumerate(self.ID_counts) if count == 1]
        remove_idx = [i for i, ids in enumerate(self.IDs) if ids in ID_remove]
        self.remove_observation(remove_idx)

        # remove duplicates (as in same ID adn same state) and keep only the most popular one
        remove_list = []
        arr_all_idx = np.asarray(self.index)
        arr_states = np.asarray(self.states)
        arr_usage = np.asarray(self.usage)
        ID_arr = np.asarray(self.IDs)
        print("Remove duplicates, keeping the one with the highest usage count")
        for ID in tqdm(self.unique_IDs):
            # which states do this ID have settings in
            state_titles = list(set(arr_states[ID_arr == ID]))
            for state in state_titles:
                # get index for ID state combo
                set_idx1 = arr_all_idx[arr_states == state]
                set_idx2 = arr_all_idx[ID_arr == ID]
                set_idx = list(set(set_idx1) & set(set_idx2))
                # if there are more than one setting to the same state and same ID
                if len(set_idx) > 1:
                    # get the usage counts
                    usage = arr_usage[set_idx]
                    # find the one with the highest usage count
                    keep = np.argmax(usage)
                    # remove highest usage from list
                    set_idx.remove(set_idx[keep])
                    # add to list to remove the duplicate with lower usage counts
                    remove_list.extend(set_idx)
        # sort before running remove observation
        print('removeing {} programs due to duplicates'.format(len(remove_list)))
        remove_list.sort()
        self.remove_observation(remove_list)

        # remove all IDs that have only 1 program again
        # (in case the state they had several programs in was the only state they used)
        ID_remove = [self.unique_IDs[i] for i, count in enumerate(self.ID_counts) if count == 1]
        remove_idx = [i for i, ids in enumerate(self.IDs) if ids in ID_remove]
        self.remove_observation(remove_idx)
