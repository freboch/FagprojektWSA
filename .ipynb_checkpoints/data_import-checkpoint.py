import numpy as np
import json


def import_data(paths, app, return_all=0):
    """
    :param paths: list with one or more filenames to the JSON file
    :param app: "EVOKE" or "MOMENT"
    :param return_all: can be 0, 1, 2, 3. See at bottom what gets returned depending on the value of return_all
    :return:
    """
    # load Data from list of paths
    data = []
    for path in paths:
        data.extend([json.loads(line) for line in open(path, 'r')])

    """ split the Data list into lists with states, IDs and settings etc. of the chosen app"""
    # initiate lists
    no_data = []
    data_IDs = []
    data_states = []
    data_settings = []
    data_envs = []
    data_intents = []
    data_progress = []
    data_duration = []
    data_usage = []
    data_matrix_m = []
    data_matrix_x = []
    data_vector_y = []
    for i, line in enumerate(data):
        # one line does not have an app_ID_hash, it is removed from the Data
        if 'app_id_hash' not in line:
            no_data.append(i)
            continue
        # if we want to only look at a specific hearing aid:
        if (app == 'EVOKE' and line['ha_family'] != 23) or (app == 'MOMENT' and line['ha_family'] != 28):
            no_data.append(i)
            continue
        # ID
        ID = line['app_id_hash']
        data_IDs.append(ID)
        # Setting
        setting = np.asarray(json.loads(line['learned_best_setting']))
        data_settings.append(setting)
        # state
        tag_dict = json.loads(line['tags'])  # dictionary with the tags
        env = tag_dict['outer']  # string with env tag
        intent = tag_dict['inner']  # list with 0, 1 or 2 strings with intent tag
        # create the env and intent list
        data_envs.append(env)
        data_intents.append(intent)
        # create the state tag for the line
        if len(intent) == 0:
            state = env
        elif len(intent) == 1:
            state = env + '_' + intent[0]
        else:  # len(intent) == 2:
            intent.sort()  # sort alphabetically so the state name will match the one in the states list
            state = env + '_' + intent[0] + '_' + intent[1]
        data_states.append(state)

        # progress
        progress = line['progress']
        data_progress.append(progress)
        # duration days
        duration = line['duration_days']
        data_duration.append(duration)
        # usage count
        usage = line['usage_count']
        data_usage.append(usage)

        # matrix x
        mat_x = line['matrix_x']
        data_matrix_x.append(mat_x)
        # matrix m
        mat_m = line['matrix_m']
        data_matrix_m.append(mat_m)
        # matrix x
        vec_y = line['vector_y']
        data_vector_y.append(vec_y)

    # remove the lines from the Data that were identified as uninteresting
    idx_delete = no_data.copy()
    data_cleaned = [val for j, val in enumerate(data) if j not in idx_delete]

    if return_all == 3:
        return data_cleaned, data_IDs, data_states, data_settings, data_envs, data_intents, data_progress, \
               data_duration, data_usage, data_matrix_x, data_matrix_m, data_vector_y
    elif return_all == 2:
        return data_cleaned, data_IDs, data_states, data_settings, data_envs, data_intents, data_progress, \
               data_duration, data_usage
    elif return_all == 1:
        return data_cleaned, data_IDs, data_states, data_settings, data_envs, data_intents
    elif return_all == 0:
        return data_cleaned, data_IDs, data_states, data_settings
