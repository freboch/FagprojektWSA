from collections import Counter
import numpy as np


# training functions
def backup_baseline():
    # can be used to fill in for empty spots in the other baselines
    B = np.random.choice([-12, 12], 1)  # Can be either -12 or 12
    M = np.random.choice([-12, 6], 1)  # Can be either -12 or 6
    T = np.random.choice([-12, 6], 1)  # Can be either -12 or 6
    predict = np.asarray([B[0], M[0], T[0]])
    return predict


def baseline_mean_train_3(all_states, state_train, settings_train, verbose=False):
    """

    :param all_states: list of strings
    :param state_train: list of strings
    :param settings_train: list of arrays
    :return: dictionary with states as keys and lists with 3 arrays as values if there was training data for the state
    otherwise an empty list
    """
    if verbose:
        print("Training mean baseline on {} observations".format(len(state_train)))
    # Make empty dictionary to contain the states and 3 mean settings
    dic = {}
    # get unique states from training data
    all_states = list(set(state_train))
    # Loop over all states
    for state in all_states:
        means = []
        # get settings for the desired state
        set_state = [setting for i, setting in enumerate(settings_train) if state_train[i] == state]
        # find mean and round to integers if there are any settings in the state
        set_state = np.asarray(set_state)
        # find mean
        mean_setting = np.rint(np.mean(set_state, 0)).astype(int)
        means.append(mean_setting)
        # find the mean of the upper half and mean of lower half relative to the found mean
        set_low = [setting for i, setting in enumerate(set_state) if np.mean(setting) < np.mean(mean_setting)]
        set_high = [setting for i, setting in enumerate(set_state) if np.mean(setting) >= np.mean(mean_setting)]
        if set_low:
            mean_low = np.rint(np.mean(np.asarray(set_low), 0)).astype(int)
            means.append(mean_low)
        if set_high:
            mean_high = np.rint(np.mean(np.asarray(set_high), 0)).astype(int)
            means.append(mean_high)
        # fill in the dictionary for the state
        dic[state] = means
    if verbose:
        print("Mean baseline finished training")
    return dic

# random corner
# is not based on training data


def baseline_most_popular_train_3(all_states, state_train, settings_train, verbose=False):
    """
    Create dictionary with the 3 most popular settings for each state in the training set
    :param all_states: list of strings
    :param state_train: list of strings
    :param settings_train: list of arrays
    :return: dictionary with states as keys and the 3 most popular settings from the state in the training data as the value.
    If there is no settings for the state, the value is an empty list
    """
    if verbose:
        print("Training most popular baseline on {} observations".format(len(state_train)))
    # Make empty dictionary to contain the states and 3 most used settings
    dic = {}
    all_states = list(set(state_train))
    # Loop over all states
    for state in all_states:
        set_state = [tuple(setting) for i, setting in enumerate(settings_train) if state_train[i] == state]
        occurrence_count = Counter(set_state)
        # Make the dictionary consist of the three most used settings in the given state
        occurrence = occurrence_count.most_common(3)
        state_set_3 = []
        for setting in occurrence:
            state_set_3.append(np.asarray(setting[0]))
        dic[state] = state_set_3
    if verbose:
        print("Most popular baseline finished training")
    return dic


# prediction functions


def baseline_mean_predict_3(training_dic, state_test, verbose=False):
    if verbose:
        print("Predicting with mean baseline on {} observations".format(len(state_test)))
    predictions = []
    bb_count = 0
    for state in state_test:
        # if the state was in training
        if state in training_dic.keys():
            predict = training_dic[state]
            # if there isnt 3 predictions from training
            if len(predict) != 3:
                for i in range(3-len(predict)):
                    predict.append(backup_baseline())
                    bb_count += 1
        else:
            predict = [backup_baseline() for i in range(3)]
            bb_count += 3
        predictions.append(predict)

    return predictions, bb_count


def baseline_most_popular_predict_3(training_dic, state_test, verbose=False):
    if verbose:
        print("Predicting with most popular baseline on {} observations".format(len(state_test)))
    predictions = []
    bb_count = 0
    for state in state_test:
        # if the state was in training
        if state in training_dic.keys():
            predict = training_dic[state]
            # if there isnt 3 predictions from training
            if len(predict) != 3:
                for i in range(3 - len(predict)):
                    predict.append(backup_baseline())
                    bb_count += 1
        else:
            predict = [backup_baseline() for i in range(3)]
            bb_count += 3
        predictions.append(predict)
    return predictions, bb_count


def baseline_random_corner_predict_3(state_test, verbose=False):
    if verbose:
        print("Predicting with random corner baseline on {} observations".format(len(state_test)))
    predictions = []
    # Make three corner predictions per new user training
    for i in range(len(state_test)):
        B = np.random.choice([-12, 12], 3)  # Can be either -12 or 12
        M = np.random.choice([-12, 6], 3)  # Can be either -12 or 6
        T = np.random.choice([-12, 6], 3)  # Can be either -12 or 6
        predict = [np.asarray([B[0], M[0], T[0]]), np.asarray([B[1], M[1], T[1]]), np.asarray([B[2], M[2], T[2]])]
        predictions.append(predict)
    return predictions
