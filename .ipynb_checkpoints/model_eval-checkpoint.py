from data_import import import_data
from create_statespace import create_statespace
from baseline import baseline_train, baseline_predict
import numpy as np

np.random.seed(42)
app = "EVOKE"

# create statespace based on chosen app
environments, intents, states = create_statespace(app)

# import Data and create lists for all 4 files and combine them
paths = ['part-00000-tid-3016944325978431049-ccd7a452-5e97-4164-94cd-373689acd9f2-6549-1-c000.json',
         'part-00001-tid-3016944325978431049-ccd7a452-5e97-4164-94cd-373689acd9f2-6550-1-c000.json',
         'part-00002-tid-3016944325978431049-ccd7a452-5e97-4164-94cd-373689acd9f2-6551-1-c000.json',
         'part-00003-tid-3016944325978431049-ccd7a452-5e97-4164-94cd-373689acd9f2-6552-1-c000.json']
data_cleaned = []
data_IDs = []
data_states = []
data_settings = []
for path in paths:
    data_cleaned1, data_IDs1, data_states1, data_settings1 = import_data(path, app)
    data_cleaned.extend(data_cleaned1)
    data_IDs.extend(data_IDs1)
    data_states.extend(data_states1)
    data_settings.extend(data_settings1)

del data_cleaned1
del data_IDs1
del data_states1
del data_settings1
del path

""" split in training and test Data """
# the number indicates the percentage that will be training Data
split = int(0.6 * len(data_cleaned))
# split
train_IDs = data_IDs[:split]
train_states = data_states[:split]
train_settings = data_settings[:split]
test_IDs = data_IDs[:split]
test_states = data_states[:split]
test_settings = data_settings[:split]


"""train and test model"""
# train the baseline model on the training Data
trained_mean = baseline_train(train_states, train_IDs, train_settings, states)

# test on the training Data
# size of train set
n = split
# collect errors
train_errors = np.zeros((n, 3))
for i in range(n):
    pred_setting = baseline_predict(train_states[i], trained_mean, states)
    error = (pred_setting-train_settings[i])**2
    train_errors[i, :] = error

# calculate the mean error on the test set
train_MSE = np.sqrt(np.mean(train_errors, 0))

# size of test set
n = len(data_cleaned)-split
# collect errors
test_errors = np.zeros((n, 3))
for i in range(n):
    pred_setting = baseline_predict(test_states[i], trained_mean, states)

    # print("the predicted setting is ", pred_setting)
    # print("the true setting was ", test_settings[i])
    error = (pred_setting-test_settings[i])**2
    test_errors[i, :] = error

# calculate the mean error on the test set
test_MSE = np.sqrt(np.mean(test_errors, 0))

print('train error is ', train_MSE)
print('test error is ', test_MSE)
