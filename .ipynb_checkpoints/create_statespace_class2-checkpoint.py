
# create statespace class
class Space:
    def __init__(self, app):
        if app == 'EVOKE':
            environments = ['car', 'cinema', 'classroom', 'home', 'largeHall', 'nature', 'noisyVenue',
                            'restaurant', 'transport', 'urban', 'work']
            intents = ['conversation', 'focus', 'leisureActivity', 'music', 'phone', 'reduceNoise', 'relax', 'tv']
        elif app == 'MOMENT':
            environments = ['dining', 'entertainment', 'familyGathering', 'outdoor', 'party', 'quiet', 'shopping',
                            'socializing', 'sport', 'transport', 'watchTv']
            intents = ['concentration', 'conversation', 'enjoyingMusic', 'enjoyingSound', 'relaxation',
                       'suppressingDisturbances']
        else:
            raise ValueError('That app can not be used to create a state space. use "MOMENT" or "EVOKE".')

        # make list of all the states
        # a state is a combined env_intent setting:
        # a combination can be 1 env and 1 intent
        # or 1 env and 2 intents
        states = []
        intents_combined = []
        for env in environments:
            tag_name = env  # the states with 0 intent chosen
            states.append(tag_name)
            for intent1 in intents:
                tag_name = env + '_' + intent1  # states with 1 intent
                states.append(tag_name)
                intents_combined.append(intent1)
                for intent2 in intents:
                    if intent1 != intent2:  # you cannot pic the same intent twice
                        # the order of the two intents is not important.
                        # Meaning home_music_relax and home_relax_music
                        # are the same state and should not both be in the list
                        tag_name1 = env + '_' + intent1 + '_' + intent2  # states with 2 intents
                        tag_name2 = env + '_' + intent2 + '_' + intent1  # states with 2 intents
                        if tag_name1 not in states and tag_name2 not in states:
                            states.append(tag_name1)
                            intents_combined.append(intent1 + '_' + intent2)

        self.environments = environments
        self.intents = intents
        self.states = states
        self.intents_combined = intents_combined
