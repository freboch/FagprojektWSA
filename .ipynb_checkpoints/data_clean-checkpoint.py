import numpy as np

def clean_data(data_cleaned, data_IDs, data_states, data_settings, data_envs, data_intents, data_progress,
               data_duration, data_usage, data_matrix_x, data_matrix_m, data_vector_y):
    N = len(data_cleaned)
    
    # unique ID
    unique_IDs, ID_counts = np.unique(np.asarray(data_IDs),return_counts=True)

    remove = []
    for i in range(N):
        # Progress
        if data_progress[i] < 30:
            remove.append(i)
        # Duration
        # duration longer than 5 years remove
        elif data_duration[i] > 365 * 5:
            remove.append(i)
        # usage count
        elif data_usage[i] < 3:
            remove.append(i)
        # Number of programs per unique ID: 20 limit
        elif ID_counts[np.where(unique_IDs == data_IDs[i])] > 20:
            remove.append(i)
        # demo-behaviour in y-vector
        elif np.all(np.asarray(data_vector_y[i])==0.5):
            remove.append(i)

    for index in sorted(remove, reverse=True):
        del data_cleaned[index]
        del data_IDs[index]
        del data_states[index]
        del data_settings[index]
        del data_envs[index]
        del data_intents[index]
        del data_progress[index]
        del data_duration[index]
        del data_usage[index]
        del data_matrix_x[index]
        del data_matrix_m[index]
        del data_vector_y[index]


    return data_cleaned, data_IDs, data_states, data_settings, data_envs, data_intents, data_progress, \
           data_duration, data_usage, data_matrix_x, data_matrix_m, data_vector_y
