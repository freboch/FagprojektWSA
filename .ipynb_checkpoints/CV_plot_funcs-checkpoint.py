import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
sns.set_theme(style="whitegrid")


def plot_3in1_RMSE(Train_errorsB1, Train_errorsB2, Train_errorsB3, Test_errorsB1,Test_errorsB2,Test_errorsB3, \
                   train_RMSE_sim, test_RMSE_sim, train_RMSE_matfac, test_RMSE_matfac):
    ticks = ["Bass", "Mid range", "Treble"]
    xtr = [0.5, 2.5, 4.5]
    x2tr = [0.7, 2.7, 4.7]
    x3tr = [0.9, 2.9, 4.9]
    x4tr = [1.1, 3.1, 5.1]
    x5tr = [1.3, 3.3, 5.3]
    xte = [0.6, 2.6, 4.6]
    x2te = [0.8, 2.8, 4.8]
    x3te = [1, 3, 5]
    x4te = [1.2, 3.2, 5.2]
    x5te = [1.4, 3.4, 5.4]

    plt.plot(x3tr, np.mean(Train_errorsB3, 0), 'o', color='#CCCC00', markersize=8, label='Popular train')
    plt.errorbar(x3tr, np.mean(Train_errorsB3, 0), fmt='none', yerr=2 * np.std(Train_errorsB3), ecolor='#CCCC00', lw=2)

    plt.plot(x3te, np.mean(Test_errorsB3, 0), 'o', color='#FF9933', markersize=8, label='Popular test')
    plt.errorbar(x3te, np.mean(Test_errorsB3, 0), fmt='none', yerr=2 * np.std(Test_errorsB3), ecolor='#FF9933', lw=2)

    plt.plot(xtr, np.mean(Train_errorsB1, 0), 'o', color='#FF95FF', markersize=8, label='Mean train')
    plt.errorbar(xtr, np.mean(Train_errorsB1, 0), fmt='none', yerr=2 * np.std(Train_errorsB1), ecolor='#FF95FF', lw=2)

    plt.plot(xte, np.mean(Test_errorsB1, 0), 'o', color='#A500D7', markersize=8, label='Mean test')
    plt.errorbar(xte, np.mean(Test_errorsB1, 0), fmt='none', yerr=2 * np.std(Test_errorsB1), ecolor='#A500D7', lw=2)

    plt.plot(x2tr, np.mean(Train_errorsB2, 0), 'o', color='#66ff66', markersize=8, label='Random train')
    plt.errorbar(x2tr, np.mean(Train_errorsB2, 0), fmt='none', yerr=2 * np.std(Train_errorsB2), ecolor='#66ff66', lw=2)

    plt.plot(x2te, np.mean(Test_errorsB2, 0), 'o', color='#009900', markersize=8, label='Random test')
    plt.errorbar(x2te, np.mean(Test_errorsB2, 0), fmt='none', yerr=2 * np.std(Test_errorsB2), ecolor='#009900', lw=2)

    plt.plot(x4tr, np.mean(train_RMSE_sim, 0), 'o', color='#65BDFF', markersize=8, label='User-based train')
    plt.errorbar(x4tr, np.mean(train_RMSE_sim, 0), fmt='none', yerr=2 * np.std(train_RMSE_sim), ecolor='#65BDFF', lw=2)

    plt.plot(x4te, np.mean(test_RMSE_sim, 0), 'o', color='#0022FF', markersize=8, label='User-based test')
    plt.errorbar(x4te, np.mean(test_RMSE_sim, 0), fmt='none', yerr=2 * np.std(test_RMSE_sim), ecolor='#0022FF', lw=2)

    plt.plot(x5tr, np.mean(train_RMSE_matfac, 0), 'o', color='#FF00AB', markersize=8, label='NMF train')
    plt.errorbar(x5tr, np.mean(train_RMSE_matfac, 0), fmt='none', yerr=2 * np.std(train_RMSE_matfac), ecolor='#FF00AB', lw=2)

    plt.plot(x5te, np.mean(test_RMSE_matfac, 0), 'o', color='#B90000', markersize=8, label='NMF test')
    plt.errorbar(x5te, np.mean(test_RMSE_matfac, 0), fmt='none', yerr=2 * np.std(test_RMSE_matfac), ecolor='#B90000', lw=2)

    # plt.grid(color='black', linestyle='-', linewidth=0.5)
    plt.xlabel('Setting', fontsize=16, fontstyle='italic')
    plt.ylabel('Root Mean Square Error', fontsize=16, fontstyle='italic')
    plt.tick_params(labelsize=13)
    plt.xticks([0.9, 2.9, 4.9], ticks)

    plt.title('RMSE from cross validation', fontsize=20, fontstyle='oblique')
    plt.legend(bbox_to_anchor=(1.05, 1), borderaxespad=0., fontsize=15)
    plt.savefig('Plots\Error_testplot_3in1.png', dpi=600, bbox_inches='tight')
    plt.show()



def plot_3in1_acc(Train_accs_mean, Train_accs_random, Train_accs_best, Test_accs_mean, Test_accs_random, \
                  Test_accs_best, Train_accs_sim, Test_accs_sim, Train_accs_matfac, Test_accs_matfac):
    Te_acc = np.transpose(np.asarray([Test_accs_mean, Test_accs_random, Test_accs_best, Test_accs_sim, Test_accs_matfac]))
    Tr_acc = np.transpose(np.asarray([Train_accs_mean, Train_accs_random, Train_accs_best, Train_accs_sim, Train_accs_matfac]))
    x = [0.9, 1.9, 2.9, 3.9, 4.9]
    x2 = [1.1, 2.1, 3.1, 4.1, 5.1]

    plt.plot(x2[2], np.mean(Tr_acc[:, 2]), 'o', color='#CCCC00', markersize=8, label='Popular train')
    plt.errorbar(x2[2], np.mean(Tr_acc[:, 2]), fmt='none', yerr=2 * np.std(Tr_acc[:, 2]), ecolor='#FF9933', lw=2)

    plt.plot(x[2], np.mean(Te_acc[:, 2]), 'o', color='#FF9933', markersize=8, label='Popular test')
    plt.errorbar(x[2], np.mean(Te_acc[:, 2]), fmt='none', yerr=2 * np.std(Te_acc[:, 2]), ecolor='#FF9933', lw=2)

    plt.plot(x2[0], np.mean(Tr_acc[:, 0]), 'o', color='#FF95FF', markersize=8, label='Mean train')
    plt.errorbar(x2[0], np.mean(Tr_acc[:, 0]), fmt='none', yerr=2 * np.std(Tr_acc[:, 0]), ecolor='#cc0066', lw=2)

    plt.plot(x[0], np.mean(Te_acc[:, 0]), 'o', color='#A500D7', markersize=8, label='Mean test')
    plt.errorbar(x[0], np.mean(Te_acc[:, 0]), fmt='none', yerr=2 * np.std(Te_acc[:, 0]), ecolor='#cc0066', lw=2)

    plt.plot(x2[1], np.mean(Tr_acc[:, 1]), 'o', color='#66ff66', markersize=8, label='Random train')
    plt.errorbar(x2[1], np.mean(Tr_acc[:, 1]), fmt='none', yerr=2 * np.std(Tr_acc[:, 1]), ecolor='#009900', lw=2)

    plt.plot(x[1], np.mean(Te_acc[:, 1]), 'o', color='#009900', markersize=8, label='Random test')
    plt.errorbar(x[1], np.mean(Te_acc[:, 1]), fmt='none', yerr=2 * np.std(Te_acc[:, 1]), ecolor='#009900', lw=2)

    plt.plot(x2[3], np.mean(Tr_acc[:, 3]), 'o', color='#65BDFF', markersize=8, label='User-based train')
    plt.errorbar(x2[3], np.mean(Tr_acc[:, 3]), fmt='none', yerr=2 * np.std(Tr_acc[:, 3]), ecolor='#65BDFF', lw=2)

    plt.plot(x[3], np.mean(Te_acc[:, 3]), 'o', color='#0022FF', markersize=8, label='User-based test')
    plt.errorbar(x[3], np.mean(Te_acc[:, 3]), fmt='none', yerr=2 * np.std(Te_acc[:, 3]), ecolor='#0022FF', lw=2)

    plt.plot(x2[4], np.mean(Tr_acc[:, 4]), 'o', color='#FF00AB', markersize=8, label='NMF train')
    plt.errorbar(x2[4], np.mean(Tr_acc[:, 4]), fmt='none', yerr=2 * np.std(Tr_acc[:, 4]), ecolor='#FF00AB', lw=2)

    plt.plot(x[4], np.mean(Te_acc[:, 4]), 'o', color='#B90000', markersize=8, label='NMF test')
    plt.errorbar(x[4], np.mean(Te_acc[:, 4]), fmt='none', yerr=2 * np.std(Te_acc[:, 4]), ecolor='#B90000', lw=2)

    # plt.grid(color='black', linestyle='-', linewidth=0.5)
    plt.ylabel('Accuracy', fontsize=16, fontstyle='italic')
    plt.xlabel('Model', fontsize=16, fontstyle='italic')
    plt.xticks([1, 2, 3, 4, 5], ['Mean', 'Random', 'Popular', 'User-based', 'NMF'])  # ,rotation=20)
    plt.tick_params(labelsize=13)
    plt.title('Accuracy from cross-validation', fontsize=20, fontstyle='oblique')
    plt.legend(bbox_to_anchor=(1.05, 1), borderaxespad=0., fontsize=15)
    plt.savefig('Plots\All_accuracy3in1.png', dpi=600, bbox_inches='tight')
    plt.show()




def plot_kfold_acc(folds, Train_accs_best, Test_accs_best, Train_accs_mean, Test_accs_mean, \
                   Train_accs_random, Test_accs_random, Train_accs_sim, Test_accs_sim,\
                   Train_accs_matfac, Test_accs_matfac):
    fig, ax1 = plt.subplots(1, 1, sharey=True, figsize=(6, 4))
    sns.lineplot(x=folds, y=Train_accs_best, label="Popular train", color='#CCCC00')
    sns.lineplot(x=folds, y=Test_accs_best, label="Popular test", color='#FF9933')
    sns.lineplot(x=folds, y=Train_accs_mean, label="Mean train", color='#FF95FF')
    sns.lineplot(x=folds, y=Test_accs_mean, label="Mean test", color='#A500D7')
    sns.lineplot(x=folds, y=Train_accs_random, label="Random train", color='#66ff66')
    sns.lineplot(x=folds, y=Test_accs_random, label="Random test", color='#009900')
    sns.lineplot(x=folds, y=Train_accs_sim, label="User-based train", color='#65BDFF')
    sns.lineplot(x=folds, y=Test_accs_sim, label="User-based test", color='#0022FF')
    sns.lineplot(x=folds, y=Train_accs_matfac, label="NMF train", color='#FF00AB')
    sns.lineplot(x=folds, y=Test_accs_matfac, label="NMF test", color='#B90000')
    ax1.set_xlabel('Folds',fontsize=16)
    ax1.set_ylabel('Accuracy',fontsize=16)
    ax1.tick_params(labelsize=13)
    ax1.legend(bbox_to_anchor=(1.05, 1), borderaxespad=0.,fontsize=15)
    ax1.axes.set_title("Acccuracy across cross validation folds", fontsize=20)
    plt.savefig('Plots\kfold_accuracy.png', dpi=600, bbox_inches='tight')
    plt.show()


def plot_kfold_RMSE(folds, Train_RMSEs_best, Test_RMSEs_best, Train_RMSEs_mean, Test_RMSEs_mean,\
                    Train_RMSEs_random, Test_RMSEs_random,Train_RMSEs_sim, Test_RMSEs_sim,\
                    Train_RMSEs_matfac, Test_RMSEs_matfac):
    fig, ax1 = plt.subplots(1, 1, figsize=(6, 4))
    sns.lineplot(x=folds, y=np.mean(Train_RMSEs_best, 1), label="Popular train", color='#CCCC00')
    sns.lineplot(x=folds, y=np.mean(Test_RMSEs_best, 1), label="Popular test", color='#FF9933')
    sns.lineplot(x=folds, y=np.mean(Train_RMSEs_mean, 1), label="Mean train", color='#FF95FF')
    sns.lineplot(x=folds, y=np.mean(Test_RMSEs_mean, 1), label="Mean test", color='#A500D7')
    sns.lineplot(x=folds, y=np.mean(Train_RMSEs_random, 1), label="Random train", color='#66ff66')
    sns.lineplot(x=folds, y=np.mean(Test_RMSEs_random, 1), label="Random test", color='#009900')
    sns.lineplot(x=folds, y=np.mean(Train_RMSEs_sim, 1), label="User-based train", color='#65BDFF')
    sns.lineplot(x=folds, y=np.mean(Test_RMSEs_sim, 1), label="User-based test", color='#0022FF')
    sns.lineplot(x=folds, y=np.mean(Train_RMSEs_matfac, 1), label="NMF train", color='#FF00AB')
    sns.lineplot(x=folds, y=np.mean(Test_RMSEs_matfac, 1), label="NMF test", color='#B90000')
    ax1.set_xlabel('Folds',fontsize=16)
    ax1.set_ylabel('RMSE',fontsize=16)
    ax1.legend(bbox_to_anchor=(1.05, 1), borderaxespad=0., fontsize=15)
    ax1.tick_params(labelsize=13)
    ax1.axes.set_title("RMSE across cross validation folds",fontsize=20)
    plt.savefig('Plots\kfold_RMSE.png', dpi=600, bbox_inches='tight')
    plt.show()



def plot_Train_RMSE(Train_errorsB1, Train_errorsB2, Train_errorsB3):
    ticks = ["Bass", "Mid range", "Treble"]
    x = [0.9, 1.9, 2.9]
    x2 = [1, 2, 3]
    x3 = [1.1, 2.1, 3.1]

    plt.plot(x,np.mean(Train_errorsB1, 0),'o', color='#ff66b3', markersize=8)
    plt.errorbar(x, np.mean(Train_errorsB1, 0), fmt='none', yerr=2*np.std(Train_errorsB1), ecolor='#cc0066', lw=2)

    plt.plot(x2,np.mean(Train_errorsB2, 0),'o', color='#66ff66', markersize=8)
    plt.errorbar(x2, np.mean(Train_errorsB2, 0), fmt='none', yerr=2*np.std(Train_errorsB2), ecolor='#009900', lw=2)

    plt.plot(x3, np.mean(Train_errorsB3, 0), 'o', color='#ffa366', markersize=8)
    plt.errorbar(x3, np.mean(Train_errorsB3, 0), fmt='none', yerr=2*np.std(Train_errorsB3), ecolor='#FF9933', lw=2)

    #plt.grid(color='black', linestyle='-', linewidth=0.5)
    plt.xlabel('Setting',fontsize=16,fontstyle='italic')
    plt.ylabel('Root Mean Square Error',fontsize=16,fontstyle='italic')
    plt.tick_params(labelsize=13)
    plt.xticks([1, 2, 3], ticks)

    plt.title('Train errors - Baseline models',fontsize=20,fontstyle='oblique')
    plt.legend(['Mean Train', 'Random Train','Popular Train'], bbox_to_anchor=(1.05, 1), borderaxespad=0., fontsize=15)
    plt.savefig('Plots\Error_trainplot.png',dpi=600, bbox_inches='tight')
    plt.show()


def plot_Test_RMSE(Test_errorsB1,Test_errorsB2,Test_errorsB3):
    ticks = ["Bass", "Mid range", "Treble"]
    x = [0.9, 1.9, 2.9]
    x2 = [1, 2, 3]
    x3 = [1.1, 2.1, 3.1]

    plt.plot(x, np.mean(Test_errorsB1, 0), 'o', color='#ff66b3', markersize=8)
    plt.errorbar(x, np.mean(Test_errorsB1, 0), fmt='none', yerr=2 * np.std(Test_errorsB1), ecolor='#cc0066', lw=2)

    plt.plot(x2, np.mean(Test_errorsB2, 0), 'o', color='#66ff66', markersize=8)
    plt.errorbar(x2, np.mean(Test_errorsB2, 0), fmt='none', yerr=2 * np.std(Test_errorsB2), ecolor='#009900', lw=2)

    plt.plot(x3, np.mean(Test_errorsB3, 0), 'o', color='#ffa366', markersize=8)
    plt.errorbar(x3, np.mean(Test_errorsB3, 0), fmt='none', yerr=2 * np.std(Test_errorsB3), ecolor='#FF9933', lw=2)

    #plt.grid(color='black', linestyle='-', linewidth=0.5)
    plt.xlabel('Setting',fontsize=16,fontstyle='italic')
    plt.ylabel('Root Mean Square Error',fontsize=16,fontstyle='italic')
    plt.tick_params(labelsize=13)
    plt.xticks([1, 2, 3], ticks)

    plt.title('Test errors - Baseline models',fontsize=20,fontstyle='oblique')
    plt.legend(['Mean Test', 'Random Test','Popular Test'], bbox_to_anchor=(1.05, 1), borderaxespad=0., fontsize=15)
    plt.savefig('Plots\Error_testplot.png',dpi=600, bbox_inches='tight')
    plt.show()

    def plot_train_acc(Train_accs_mean, Train_accs_random, Train_accs_best):
        Tr_acc = np.transpose(np.asarray([Train_accs_mean, Train_accs_random, Train_accs_best]))
        x = [0, 1, 2]

        plt.plot(x[0], np.mean(Tr_acc[:, 0]), 'o', color='#ff66b3', markersize=8)
        plt.errorbar(x[0], np.mean(Tr_acc[:, 0]), fmt='none', yerr=2 * np.std(Tr_acc[:, 0]), ecolor='#cc0066', lw=2)

        plt.plot(x[1], np.mean(Tr_acc[:, 1]), 'o', color='#66ff66', markersize=8)
        plt.errorbar(x[1], np.mean(Tr_acc[:, 1]), fmt='none', yerr=2 * np.std(Tr_acc[:, 1]), ecolor='#009900', lw=2)

        plt.plot(x[2], np.mean(Tr_acc[:, 2]), 'o', color='#ffa366', markersize=8)
        plt.errorbar(x[2], np.mean(Tr_acc[:, 2]), fmt='none', yerr=2 * np.std(Tr_acc[:, 2]), ecolor='#FF9933', lw=2)

        # plt.grid(color='black', linestyle='-', linewidth=0.5)
        plt.ylabel('Accuracy', fontsize=16, fontstyle='italic')
        plt.xlabel('Model', fontsize=16, fontstyle='italic')
        plt.xticks([0, 1, 2], ['Mean', 'Random', 'Popular'])  # ,rotation=20)
        plt.title('Train accuracy Baseline models', fontsize=20, fontstyle='oblique')
        plt.tick_params(labelsize=13)
        plt.legend(['Mean', 'Random', 'Popular'], bbox_to_anchor=(1.05, 1), borderaxespad=0., fontsize=15)
        plt.savefig('Plots\Train_accuracy.png', dpi=600, bbox_inches='tight')
        plt.show()

    def plot_test_acc(Test_accs_mean, Test_accs_random, Test_accs_best):
        Te_acc = np.transpose(np.asarray([Test_accs_mean, Test_accs_random, Test_accs_best]))
        x = [0, 1, 2]

        plt.plot(x[0], np.mean(Te_acc[:, 0]), 'o', color='#ff66b3', markersize=8)
        plt.errorbar(x[0], np.mean(Te_acc[:, 0]), fmt='none', yerr=2 * np.std(Te_acc[:, 0]), ecolor='#cc0066', lw=2)

        plt.plot(x[1], np.mean(Te_acc[:, 1]), 'o', color='#66ff66', markersize=8)
        plt.errorbar(x[1], np.mean(Te_acc[:, 1]), fmt='none', yerr=2 * np.std(Te_acc[:, 1]), ecolor='#009900', lw=2)

        plt.plot(x[2], np.mean(Te_acc[:, 2]), 'o', color='#ffa366', markersize=8)
        plt.errorbar(x[2], np.mean(Te_acc[:, 2]), fmt='none', yerr=2 * np.std(Te_acc[:, 2]), ecolor='#FF9933', lw=2)

        # plt.grid(color='black', linestyle='-', linewidth=0.5)
        plt.ylabel('Accuracy', fontsize=16, fontstyle='italic')
        plt.xlabel('Model', fontsize=16, fontstyle='italic')
        plt.xticks([0, 1, 2], ['Mean', 'Random', 'Popular'])  # ,rotation=20)
        plt.tick_params(labelsize=13)
        plt.title('Test accuracy Baseline models', fontsize=20, fontstyle='oblique')
        plt.legend(['Mean', 'Random', 'Popular'], bbox_to_anchor=(1.05, 1), borderaxespad=0., fontsize=15)
        plt.savefig('Plots\Test_accuracy.png', dpi=600, bbox_inches='tight')
        plt.show()
