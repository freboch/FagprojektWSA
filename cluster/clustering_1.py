from data_class import *
from create_statespace_class import *

import matplotlib.pyplot as plt
import pandas as pd
# matplotlib inline
import numpy as np

np.random.seed(42)
# choose which app to look at between 'EVOKE' and 'MOMENT'
app = "EVOKE"

# get the data
paths = ['part-00000-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5531-1-c000.json',
         'part-00001-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5532-1-c000.json',
         'part-00002-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5533-1-c000.json',
         'part-00003-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5534-1-c000.json']

# create the data object
data = Data(paths, app)

# clean the data
data.clean()

# create the state space
space = Space(app)

# create matrix for [Bass, Midrange, Treble] settings (they are currently in a list)
setting_vector = np.ones((len(data.settings), 3))
setting_vector[:] = None

for i, setting in enumerate(data.settings):
    setting_vector[i, 0] = setting[0]
    setting_vector[i, 1] = setting[1]
    setting_vector[i, 2] = setting[2]

setting_vector = setting_vector.astype(int)

# split the settings based on state
sets = []
dts = np.asarray(data.states)
for state in space.states:
    set_state = setting_vector[dts == state]
    sets.append(set_state)

X = np.zeros((len(space.states), 25*19*19))
for state in space.states:
    # get index for desired state
    index = space.states.index(state)
    set_state = sets[index]

    bass = set_state[:,0]
    midrange = set_state[:,1]
    treble = set_state[:,2]

    # count how many times each setting occurs

    state_matrix = np.zeros((25, 19, 19))
    for setting in set_state:
        state_matrix[setting[0]+12,setting[1]+12,setting[2]+12] += 1

    state_array = np.ravel(state_matrix)
    X[index,:] = state_array
end = 0

from sklearn.cluster import AgglomerativeClustering

cluster = AgglomerativeClustering(n_clusters=20, affinity='euclidean', linkage='ward')
cluster.fit_predict(X)

print(cluster.labels_)
begin = 0

from scipy.cluster.hierarchy import dendrogram, linkage
from matplotlib import pyplot as plt

linked = linkage(X, 'single')

labelList = space.states

plt.figure(figsize=(10, 7))
dendrogram(linked,
            orientation='top',
            labels=labelList,
            distance_sort='descending',
            show_leaf_counts=True)
plt.show()

end = 1