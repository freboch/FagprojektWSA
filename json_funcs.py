import json
import os


def open_json(name, directory):
    my_path = os.getcwd()
    path = os.path.join(my_path, directory, name+'.json')
    file = open(path)
    data = json.loads(file.read())
    return data


def write_json(file, name, directory):
    # write JSON
    json_file = json.dumps(file)
    my_path = os.getcwd()
    path = os.path.join(my_path, directory, name+'.json')
    f = open(path, 'w')
    f.write(json_file)
    f.close()
