import pandas as pd

from transform_functions import *
from nmf import *
from baseline_models_funcs_count import backup_baseline

import numpy as np


def train_matrix_fac(train_IDs, train_states, train_settings, verbose=False):
    if verbose:
        print("Training Matrix factorization on {} observations".format(len(train_states)))
    # get unique IDs and states
    unique_IDs = np.asarray(list(set(train_IDs)))
    unique_states = np.asarray(list(set(train_states)))
    # transform setting to non-negative
    trans_settings = transform_data('positive', train_settings)
    # initiate matrices
    mat_B = np.zeros((len(unique_IDs), len(unique_states)))
    mat_M = np.zeros((len(unique_IDs), len(unique_states)))
    mat_T = np.zeros((len(unique_IDs), len(unique_states)))
    # fill matrices
    for i, setting in enumerate(trans_settings):
        bass = setting[0]
        midrange = setting[1]
        treble = setting[2]
        ID = list(unique_IDs).index(train_IDs[i])
        state = list(unique_states).index(train_states[i])
        mat_B[ID, state] = bass
        mat_M[ID, state] = midrange
        mat_T[ID, state] = treble

    # rank for factorisation
    rank = 3
    # collect the three matrices in 1
    matrix = np.hstack([mat_B, mat_M, mat_T])

    # factorize the combined matrix
    max_iter = 100
    # W : Obtained factor matrix, shape (m,k)
    # H : Obtained coefficient matrix, shape (n,k)
    W, H, info = NMF_HALS().run(matrix, rank, max_iter=max_iter, verbose=-1)
    fac_mat = np.dot(W, H.T)
    fac_mat_round = np.round(fac_mat)
    p_B, p_M, p_T = fac_mat_round[:, :len(unique_states)], \
                    fac_mat_round[:, len(unique_states):2*len(unique_states)], \
                    fac_mat_round[:, 2*len(unique_states):]

    # factorize the combined matrix
    max_iter = 1000
    # W : Obtained factor matrix, shape (m,k)
    # H : Obtained coefficient matrix, shape (n,k)
    W, H, info = NMF_HALS().run(matrix, rank, max_iter=max_iter, verbose=-1)
    fac_mat = np.dot(W, H.T)
    fac_mat_round = np.round(fac_mat)
    p_B1, p_M1, p_T1 = fac_mat_round[:, :len(unique_states)], \
                    fac_mat_round[:, len(unique_states):2 * len(unique_states)], \
                    fac_mat_round[:, 2 * len(unique_states):]

    # factorize the combined matrix
    max_iter = 5000
    # W : Obtained factor matrix, shape (m,k)
    # H : Obtained coefficient matrix, shape (n,k)
    W, H, info = NMF_HALS().run(matrix, rank, max_iter=max_iter, verbose=-1)
    fac_mat = np.dot(W, H.T)
    fac_mat_round = np.round(fac_mat)
    p_B2, p_M2, p_T2 = fac_mat_round[:, :len(unique_states)], \
                    fac_mat_round[:, len(unique_states):2 * len(unique_states)], \
                    fac_mat_round[:, 2 * len(unique_states):]

    # make a dataframe to contain the values
    data = []
    for i in range(len(unique_IDs)):
        row = []
        for j in range(len(unique_states)):
            j_sett = []
            B = p_B[i, j]
            M = p_M[i, j]
            T = p_T[i, j]
            setting = transform_data('negative', [B, M, T])
            j_sett.append(setting)
            B1 = p_B1[i, j]
            M1 = p_M1[i, j]
            T1 = p_T1[i, j]
            setting1 = transform_data('negative', [B1, M1, T1])
            # only append if it is nan or a setting different from the first appended setting
            if setting1 is np.nan:
                j_sett.append(setting1)
            elif setting is np.nan:
                j_sett.append(setting1)
            elif np.any(setting != setting1):
                j_sett.append(setting1)
            else:
                j_sett.append(np.nan)
            B2 = p_B2[i, j]
            M2 = p_M2[i, j]
            T2 = p_T2[i, j]
            setting2 = transform_data('negative', [B2, M2, T2])
            if setting2 is np.nan:
                j_sett.append(setting2)
            elif setting is np.nan and setting1 is np.nan:
                j_sett.append(setting2)
            elif setting1 is np.nan and np.any(setting != setting2):
                j_sett.append(setting2)
            elif setting is np.nan and np.any(setting1 != setting2):
                j_sett.append(setting2)
            elif np.any(setting != setting2) and np.any(setting1 != setting2):
                j_sett.append(setting2)
            else:
                j_sett.append(np.nan)

            row.append(j_sett)
        data.append(row)

    facmat_df = pd.DataFrame(data, columns=unique_states, index=unique_IDs)
    if verbose:
        print("Matrix factorization finished training")
    return facmat_df, unique_states, unique_IDs


def predict_matrix_fac(df_train, state_test, IDs_test):
    facmat_df = pd.DataFrame(df_train[0], columns=df_train[1], index=df_train[2])
    train_unique_IDs = facmat_df.axes[0]
    train_unique_states = facmat_df.axes[1]
    predictions = []
    bb_count = 0
    for i in range(len(state_test)):
        state = state_test[i]
        ID = IDs_test[i]
        # if the state and ID is represented in the training set
        setting = []
        if ID in train_unique_IDs and state in train_unique_states:
            # get the settings from the training dataframe
            setts = facmat_df[state][ID]
            # get the settings from matfac into the predictions
            for sett in setts:
                # check if the setting is nan
                if sett is np.nan:
                    setting.append(backup_baseline())
                    bb_count += 1
                else:
                    setting.append(np.asarray(sett))
        else:  # if they are not represented in the training set make all 3 random
            for k in range(3):
                setting.append(backup_baseline())
                bb_count += 1
        predictions.append(setting)

    return predictions, bb_count
