import numpy as np


# evaluate a model that gives a top 3 prediction

def evaluate_acc_3(test_settings, predictions):
    accuracy_count = 0
    # loop over all predictions for all states
    for i, predict in enumerate(predictions):
        # Calculate accuracy
        if np.all(predict[0] == test_settings[i]):
            accuracy_count += 1
        elif np.all(predict[1] == test_settings[i]):
            accuracy_count += 1
        elif np.all(predict[2] == test_settings[i]):
            accuracy_count += 1

    accuracy = accuracy_count/len(test_settings)
    return accuracy


def evaluate_RMSE_3(test_settings, predictions):
    errors = []
    # loop over all predictions for all states
    for i, predict in enumerate(predictions):
        # Calculate RMSE
        pred_errors = []
        for top_3 in predict:
            pred_errors.append(list((top_3 - test_settings[i]) ** 2))
        min_list = min(pred_errors, key=sum)
        errors.append(min_list)

    RMSE = list(np.sqrt(np.mean(errors, 0)))
    return RMSE



