import numpy as np
import pandas as pd
from sklearn.metrics.pairwise import nan_euclidean_distances
from baseline_models_funcs_count import backup_baseline
from tqdm import tqdm


# training function


def user_item_matrix(user_id, item_id, sett):
    # Create the user-item matrix (train set only)
    # Make user-item matrix from df (train)
    df = pd.DataFrame(list(zip(user_id, item_id, sett)), columns=["user_id", "item_id", "sett"])
    train_data_matrix = df.pivot(index="user_id", columns="item_id", values="sett")
    return train_data_matrix


def similarity_matrix(user_id, item_id, sett):
    # Create the similarity matrix (train set only)
    # Loop over settings
    l = len(sett)
    # B
    sett1 = []
    for i in range(l):
        x = sett[i][0]
        sett1.append(x)
    # M
    sett2 = []
    for i in range(l):
        y = sett[i][1]
        sett2.append(y)
    # T
    sett3 = []
    for i in range(l):
        z = sett[i][2]
        sett3.append(z)

    # Make dataframe with all relevant variables
    df1 = pd.DataFrame(list(zip(user_id, item_id, sett1)), columns=["user_id", "item_id", "sett1"])
    df2 = pd.DataFrame(list(zip(user_id, item_id, sett2)), columns=["user_id", "item_id", "sett2"])
    df3 = pd.DataFrame(list(zip(user_id, item_id, sett3)), columns=["user_id", "item_id", "sett3"])

    # Create user-item matrices
    nan = float("NaN")
    train_data_matrix_1 = df1.pivot(index="user_id", columns="item_id", values="sett1")
    train_data_matrix_2 = df2.pivot(index="user_id", columns="item_id", values="sett2")
    train_data_matrix_3 = df3.pivot(index="user_id", columns="item_id", values="sett3")

    # Compute Euclidean distance
    dist1 = nan_euclidean_distances(train_data_matrix_1.values)
    dist2 = nan_euclidean_distances(train_data_matrix_2.values)
    dist3 = nan_euclidean_distances(train_data_matrix_3.values)

    # Take mean and thereby create one final similarity matrix
    sim_matrix = (dist1 + dist2 + dist3) / 3

    # Save similarity matrix
    np.savetxt("sim_matrix_try.csv", sim_matrix, delimiter=",")

    return sim_matrix


def train_similarity(train_IDs, train_states, train_settings, verbose=False):
    if verbose:
        print("Training similarity model")
    user_id = np.asarray(train_IDs)
    item_id = np.asarray(train_states)
    sett = np.asarray(train_settings)

    train_data_matrix = user_item_matrix(user_id, item_id, sett)
    sim_matrix = similarity_matrix(user_id, item_id, sett)
    if verbose:
        print("Finished training similarity model")
    return sim_matrix, train_data_matrix


def prediction_similarity(ID_test, state_test, sim_matrix, train_data_matrix):
    print("Computing predictions for similarity model")
    predictions = []
    bb_count = 0
    for k in tqdm(range(len(ID_test))):
        user_id = ID_test[k]
        item_id = state_test[k]
        # Make top 3 predictions for a specific user in a desired state
        # check if user is represented in the training data
        if user_id in train_data_matrix.axes[0] and item_id in train_data_matrix.axes[1]:
            # Find users index in similarity matrix (and user_item_matrix)
            index = np.nonzero(train_data_matrix.index == user_id)[0][0]
            # Find most similar users to the user in question
            sorted_index = np.argsort(sim_matrix[index, :])
            # Assumption: Not necessary to remove the user itself
            # Column for state of interest (item_id)
            state = train_data_matrix[item_id]
            # If most similar users have a sett. in the desired state (item_id) this will be the predicted sett.
            # Loop over most similar users
            pred_sett = []
            for i in sorted_index:
                # Append to list if prediction is not NaN and list is empty
                if type(state[i]) == np.ndarray and len(pred_sett) == 0:
                    pred_sett.append(list(state[i]))
                # Append to list if prediction is not already in list
                if type(state[i]) == np.ndarray and len(pred_sett) != 0:
                    if not list(state[i]) in pred_sett:
                        pred_sett.append(list(state[i]))
                # Stop if we have top 3 predictions
                if len(pred_sett) == 3:
                    break

            # If there are not 3 predictions, append sett. from backup baseline (random corner)
            if len(pred_sett) != 3:
                for k in range(3 - len(pred_sett)):
                    random = backup_baseline()
                    bb_count += 1
                    random = list(random)
                    pred_sett.append(random)
        else:  # if the user and/or state is not it the matrix from the training data
            pred_sett = []
            for j in range(3):
                random = backup_baseline()
                bb_count += 1
                pred_sett.append(list(random))

        # Transform back to arrays
        pred_sett[0] = np.array(pred_sett[0])
        pred_sett[1] = np.array(pred_sett[1])
        pred_sett[2] = np.array(pred_sett[2])
        predictions.append(pred_sett)
    return predictions, bb_count
