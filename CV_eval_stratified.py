from data_class import *
from create_statespace_class import *
from baseline_models_funcs_count import *
from evaluate_models_funcs import *
from similarity_funcs import *
from matrix_fac_funcs import *
from json_funcs import *

import numpy as np
import pandas as pd
from sklearn.model_selection import StratifiedKFold # import KFold

np.random.seed(42)

# choose which app to look at between 'EVOKE' and 'MOMENT'
app = "EVOKE"
# get the data
paths = ['part-00000-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5531-1-c000.json',
         'part-00001-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5532-1-c000.json',
         'part-00002-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5533-1-c000.json',
         'part-00003-tid-5172624987854127169-b1684884-bebf-43d9-bb55-f03708dbec1f-5534-1-c000.json']

# create the data object
data = Data(paths, app)
# create the state space
space = Space(app)
# clean the data
data.clean()
data.CF_clean()

# begin cross validation
k = 10
# Define the splits
kf = StratifiedKFold(n_splits=k, random_state=42, shuffle=True)
fold = 1
# Get the number of splitting iterations for the data set
# use the unique IDs so the same user wont show up in both training and test set
data_ID_array = np.asarray(data.IDs)
data_states_array = np.asarray(data.states)
data_settings_array = np.asarray(data.settings)

kf.get_n_splits(data_settings_array, data_ID_array)

# save all the error and accuracy lists to a dictionary before json dumping it
eval_data_dic = {
    'Train_RMSEs_mean': [],
    'Test_RMSEs_mean': [],
    'Train_RMSEs_best': [],
    'Test_RMSEs_best': [],
    'Train_RMSEs_random': [],
    'Test_RMSEs_random': [],
    'Train_accs_similarity': [],
    'Train_RMSEs_similarity': [],
    'Train_RMSEs_mat_fac': [],
    'Test_RMSEs_mat_fac': [],
    'Train_accs_mean': [],
    'Test_accs_mean': [],
    'Train_accs_best': [],
    'Test_accs_best': [],
    'Train_accs_random': [],
    'Test_accs_random': [],
    'Test_accs_similarity': [],
    'Test_RMSEs_similarity': [],
    'Train_accs_mat_fac': [],
    'Test_accs_mat_fac': []
}

backup_base_count_dic = {
    'Train_fold_size': [],
    'Test_fold_size': [],
    'Train_mat_fac': [],
    'Test_mat_fac': [],
    'Train_mean': [],
    'Test_mean': [],
    'Train_best': [],
    'Test_best': [],
    'Train_sim': [],
    'Test_sim': []
}

# run cross validation
for train_index, test_index in kf.split(data_settings_array, data_ID_array):
    print('CV fold {}'.format(fold))
    fold += 1
    # train
    train_states = data_states_array[train_index]
    train_settings = data_settings_array[train_index]
    train_IDs = data_ID_array[train_index]
    # test
    test_states = data_states_array[test_index]
    test_settings = data_settings_array[test_index]
    test_IDs = data_ID_array[test_index]

    backup_base_count_dic['Train_fold_size'].append(len(train_index))
    backup_base_count_dic['Test_fold_size'].append(len(test_index))

    # NMF
    # train models on training data
    facmat_df = train_matrix_fac(train_IDs, train_states, train_settings, verbose=True)
    # train predictions
    train_facmat_predictions, bb_count = predict_matrix_fac(facmat_df, train_states, train_IDs)
    backup_base_count_dic['Train_mat_fac'].append(bb_count)
    eval_data_dic['Train_accs_mat_fac'].append(evaluate_acc_3(train_settings, train_facmat_predictions))
    eval_data_dic['Train_RMSEs_mat_fac'].append(evaluate_RMSE_3(train_settings, train_facmat_predictions))
    # test predictions
    test_facmat_predictions, bb_count = predict_matrix_fac(facmat_df, test_states, test_IDs)
    backup_base_count_dic['Test_mat_fac'].append(bb_count)
    eval_data_dic['Test_accs_mat_fac'].append(evaluate_acc_3(test_settings, test_facmat_predictions))
    eval_data_dic['Test_RMSEs_mat_fac'].append(evaluate_RMSE_3(test_settings, test_facmat_predictions))

    # mean
    # train models on training data
    mean_dic = baseline_mean_train_3(space.states, train_states, train_settings, verbose=True)
    # train predictions
    train_mean_predictions, bb_count = baseline_mean_predict_3(mean_dic, train_states)
    backup_base_count_dic['Train_mean'].append(bb_count)
    eval_data_dic['Train_accs_mean'].append(evaluate_acc_3(train_settings, train_mean_predictions))
    eval_data_dic['Train_RMSEs_mean'].append(evaluate_RMSE_3(train_settings, train_mean_predictions))
    # test predictions
    test_mean_predictions, bb_count = baseline_mean_predict_3(mean_dic, test_states)
    backup_base_count_dic['Test_mean'].append(bb_count)
    eval_data_dic['Test_accs_mean'].append(evaluate_acc_3(test_settings, test_mean_predictions))
    eval_data_dic['Test_RMSEs_mean'].append(evaluate_RMSE_3(test_settings, test_mean_predictions))

    # most popular
    # train models on training data
    best_dic = baseline_most_popular_train_3(space.states, train_states, train_settings, verbose=True)
    # train predictions
    train_best_predictions, bb_count = baseline_most_popular_predict_3(best_dic, train_states)
    backup_base_count_dic['Train_best'].append(bb_count)
    eval_data_dic['Train_accs_best'].append(evaluate_acc_3(train_settings, train_best_predictions))
    eval_data_dic['Train_RMSEs_best'].append(evaluate_RMSE_3(train_settings, train_best_predictions))
    # test predictions
    test_best_predictions, bb_count = baseline_most_popular_predict_3(best_dic, test_states)
    backup_base_count_dic['Test_best'].append(bb_count)
    eval_data_dic['Test_accs_best'].append(evaluate_acc_3(test_settings, test_best_predictions))
    eval_data_dic['Test_RMSEs_best'].append(evaluate_RMSE_3(test_settings, test_best_predictions))

    # user based
    # train models on training data
    sim_matrix, train_data_matrix = train_similarity(train_IDs, train_states, train_settings, verbose=True)
    # train predictions
    train_similarity_predictions, bb_count = prediction_similarity(train_IDs, train_states, sim_matrix, train_data_matrix)
    backup_base_count_dic['Train_sim'].append(bb_count)
    eval_data_dic['Train_accs_similarity'].append(evaluate_acc_3(train_settings, train_similarity_predictions))
    eval_data_dic['Train_RMSEs_similarity'].append(evaluate_RMSE_3(train_settings, train_similarity_predictions))
    # test predictions
    test_similarity_predictions, bb_count = prediction_similarity(test_IDs, test_states, sim_matrix, train_data_matrix)
    backup_base_count_dic['Test_sim'].append(bb_count)
    eval_data_dic['Test_accs_similarity'].append(evaluate_acc_3(test_settings, test_similarity_predictions))
    eval_data_dic['Test_RMSEs_similarity'].append(evaluate_RMSE_3(test_settings, test_similarity_predictions))

    # random
    # train predictions
    train_random_predictions = baseline_random_corner_predict_3(train_states)
    eval_data_dic['Train_accs_random'].append(evaluate_acc_3(train_settings, train_random_predictions))
    eval_data_dic['Train_RMSEs_random'].append(evaluate_RMSE_3(train_settings, train_random_predictions))
    # test predictions
    test_random_predictions = baseline_random_corner_predict_3(test_states)
    eval_data_dic['Test_accs_random'].append(evaluate_acc_3(test_settings, test_random_predictions))
    eval_data_dic['Test_RMSEs_random'].append(evaluate_RMSE_3(test_settings, test_random_predictions))

# write results to json
folder = 'CV_files'
write_json(eval_data_dic, 'eval_data_dic_all_max_iter_stratified', folder)
write_json(backup_base_count_dic, 'backup_base_count_dic_max_iter_stratified', folder)

